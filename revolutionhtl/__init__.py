"""

REvolutionH-tl
--------------

Reconciliation of Evolutionary Histories TooL

Classes and modules for inference, reconciliation,
and analysis of event-labeled genes tree describing
the evolutionary history of genes among different genomes. 

To run an anlaysis you only need to use the Project class
and FASTA files containing genomes of different species.

Classes:
--------

- Project [Class]
  Helps you to configure
  and run analysis

- GenesDict [Class]
  Handles information of genes

Modules:
--------

- basic_functions [module]
  Functions used by all
  the other modules

- errors [module]
  Errors that may be raised durig analyzis

- evolutionaryTree [module]
  Classes to represent ultrametreics
  (genes and species tree) and algorithms
  over them.

- nets [module]
  Functions to read and write the orthology
  relations in custom formats.

- order [module]
  Classes to travel on evolutionaryTree.Tree
  nodes

- stoerwagner [module]
  Compute all the minimun cuts of an
  un-directed weighted graph.
  
"""

import os
import pandas as pd
import pickle
from itertools import chain

import networkx as nx
from . import errors
from . import nets as nets
from . import evolutionaryTree
from . import basic_functions

####################################
#                                  #
# Class to handle gene information #
#                                  #
####################################

class GenesDict:
    """
    Class to handle genes information
    """
    def __init__( self , genes_dict_path, **kwargs ):

        kwargs["species2genes"]= kwargs.get("species2genes",dict())
        kwargs["gene2species"]= kwargs.get("gene2species",dict())

        self.__dict__.update( kwargs )

        self.species= self.gene2species

        if( genes_dict_path!=None ):
            df= pd.read_csv( genes_dict_path , sep="\t")

            sp_cols= set(("gene","species"))
            dif= sp_cols.difference( df.columns )
            if(len(dif)>0):
                dif= ", ".join(map(lambda i : "'{}'".format(i) ,dif))
                msg=  "Missing column(s) {} in the genes_dict file".format( dif )
                raise ValueError(msg)
            
            self.__dict__.update({ col : dict() for col in df.columns if col not in self.__dict__})

            dif= set(df.columns).difference( sp_cols )

            for i, r in df.iterrows():

                gen= r["gene"]
                species= r["species"]

                self.species[gen]= species
                self.species2genes[species]= self.species2genes.get( species, set() )
                self.species2genes[species].add(gen)

                for col in dif:
                    self.__dict__[col][gen]= r[col]

        # Lo que sigue es un parche, y debe retirarse en el futuro

class FastasLabel:
    min_cols= ["file name", "species"]

    def __init__( self, fastas_label , fastas_list=None , fastas_format=None ):
        self.file2label= dict()
        self.label2file= dict()
        self.file2sequences_kind= dict()

        if( fastas_label==None ):
            if( fastas_format=="ENSEMBLE" ):
                if( fastas_list==None ):
                    msg= "If fastas_label==None and fastas_format='ENSEMBLE', then fastas_path must be provided."
                    msg+= "For detailed description see Documentation."
                    raise ValueError(msg)
                else:
                    for file in fastas_list:
                        file= file.split("/")[-1]
                        gender, species= file.split(".")[0].split("_")
                        sequencekind= file.split(".")[-3]
                        self.file2sequences_kind[file]= sequencekind
                        label= gender[0].upper()+ gender[1:] + species[0].upper()+ species[1:]
                        self.file2label[ file ]= label
                        self.label2file[ label ]= file
            else:
                msg= "Fastas label not provided. If your fastas files are in ensemble format try "
                msg+= "setting option fastas_format='ENSEMBLE'"
                raise errors.ProjectConfigurationError(msg)
        else:
            df= pd.read_csv(fastas_label)
            dif=  set(self.min_cols).difference( df.columns )
            if (len(dif)>0):
                msg= "When fastas_label is a .tsv file path, the file must contain at least {} columns:\n".format(len(self.min_cols))
                msg+= "but {} are missed".format(", ".join(dif))
                raise ValueError(msg)
            for _, (file, species) in df[min_cols].iterrows():
                self.file2label[ file ]= species
                self.label2file[ species ]= file

############################
#                          #
# Class to handle analysis #
#                          #
############################

class Project:
    """

    revolutionhtl.Project
    ---------------------

    Class that helps to handle analysis information,
    run methods, and interact with results.

    All you need is:
    - Configure your project: project= Project( [Options] )
    - Run analysis: project.run_analysis( [Options] )

    The analysis consists of 3 main steps:

    1. Orthology graph creation.
       May use edges list or infer
       from fastas files with proteinortho.

    2. Genes tree creation.
       Use Orthology graphs and optionaly
       filters.

    3. Trees reconciliation.
       Reconciliate the genes tree with an species tree
       provided or inferred.

    Each of these steps depends on some data that is indicated
    as project attributes that you can configure at input options,
    these also determine path and name of the project's
    directory.

    Project is created as a directory containg sub-directories 
    with results and project information.

    If the project does not exist, it is created.

    If the project already exist it is loaded, but you only can reset
    the project attributes if 'reset' option is True.

    The project has methods to run analysis, visualizate and interact with results.

    Options:
    --------

    - project_name [str] (default: "project")
      Name of project directory.
      The projects directory will have this name.

    - project_path [str] (default: "./")
      Absolute path where project directory will be created or loaded.
      Example: "/absolute/path/where/project/directory/will/be/created/or/loaded/"

    - reset [bool] (default: False)
      True if you wan to reset projet parameters if exist.

    - fastas_path [str]
      Absolute path to directory containg fasta files to analyze.
      Example: "/absolute/path/where/fasta/files/are/stored/"
      Will take all files with extensions:
      - .fa
      - .fasta
      - .faa
      - .FA
      - .FASTA
      - .FAA

    - fastas_label [str]
      Specifies which species correspond to heach fasta file.
      This parameter may be a reserved string or a path:
      - Reserved strings:
        - "ENSEMBLE"
          Assumes the fastas are named as the standar nomenclature of ensemble.
      - Path:
        Absolute path to .tsv file containing two columns:
        - file name
        - species
        Example path: "/absolute/path/where/is/file.tsv/"

    - genes_dict [str]
      Information of genes.
      May be created automatucally if data is avalible
      or may be provided if want to filter genes for
      custom analysis.
      - Automatic creation:
        This file will be created automatically if
        fastas_path and fastas_label provided and
        genes_dict not provided.
      - path to .tsv file
        This file must contain at least 2 columns:
        - gene ID
        - species

    - orthologs_list [str]
      Contain orthology raltions for orthology graph
      construction avoiding proteinortho. Can be specified
      with reserved strings or .tsv file:
      Reserved strings:
      - "PROTEINORTHO"
        This runs proteinortho for orthology prediction.
      Path to .tsv file containg two or three columns:
      - gene a (mandatory)
      - gene b (mandatory)
      - weight (optional)

    - blast_run [str]
      Path where are stored .bla or .diamond files to avoid
      proteinortho to create it (if it is runned).
      The file extension dependes on 'proteinortho_p_algorithm' option:
      - 'diamond' : .diamond
      - 'blastn' : .bla
      - 'blastp' : .bla

    - proteinortho_steps [list of ints]
      Steps of proteinortho to run.
      Sublist of [1,2,3]

    - proteinortho_path [string] (default: "proteinortho6.pl")
      Coommand to run proteinortho

    - proteinortho_p_algorithm [str]
      Algorithm to run secuences comparation.
      Dependes on your sequences type:
      - "diamond" : for peptides (recomended)
      - "blastp"  : for peptides
      - "blastn"  : for nucleotides

    - species_tree [str] (default: "CONSENSUS")
      Indicates species tree to use for trees reconciliation.
      It may be a reserved string or a path to a .newick file.

      Reserved string:
      - "CONSENSUS"
        Infers the species tree from genes tree using the
        mincut-SuperTree algorithm.

      path to .newick file:
      The file must contain the species tree in newick format.
      The allowed characters are:
      - from 'A' to 'Z' in upper an lower case
      - comma ','
      - parenthesis '(' and ')'
      - semicolon ';'
      - NO spaces ' '
      - NO underscore '_'
      - NO new lines
      - The species must coincide with those in the genes tree
      Example:
        The newick tree:
        "((Human,Chimpanzee),(Mouse,Rattu),Opossium);"
        represents the species tree:
        -.
         |-Opossium
         |
         |-.
         | |-Rattu
         | |-Mouse
         |
         |-.
         | |-Chimpanzee
         | |-Human

    Methods:
    --------

    - run_analysis()
      Run the analysis or specific steps.

      The steps are:
      1. Create orthology graphs
      2. Create gene trees
      3. reconciliate gene trees

      type "run_analysis?" for detailed description.
      
    - loadGenesdict()
      Returns the genesdict object with dictionaryes
      specified by 'genes_dict' attrubute.
      See also the GenesDict class

    - getTree( ID={int or str} )
      returns a Tree object asocitead to an ID.

    - get_valid_trees_ID()
      returns list of avalible trees ID

    - get_valid_trees_list()
      returns list of avalible trees

    - getNewikcFileName( ID={int or str} )
      Returns file name asociated to an ID.

    - getNewickFilePath( ID={int or str} )
      Returns file path asociated to an ID.

    - getNewickString( ID={int or str} )
      Returns a genes tree in newick format
    """

    __not_allowed_characters= [ "-", "_" , "." , " " ]

    __algorith_2_extension={"blastp" : ".bla",
                          "blastn" : ".bla",
                          "diamond" : ".diamond"}

    __project_dirs=["Graphs",
              "Trees",
              "Reconciliation",
              "Summary",
              "GenesDict",
              "Tmp",
              "Prt",
              "Blast",
              "Orthologs"]

    __project_attrs={
        "project_name" : None,
        "project_path" : None,
        "path" : None,
        "fastas_path" : None,
        "fastas_label" : None,
        "genes_dict" : None,
        "_genes_dict" : None,
        "orthologs_list" : None,
        "orthology_graph" : None,
        "reported_edges" : None,
        "blast_run" : None,
        "proteinortho_steps" : None,
        "proteinortho_path" : None,
        "proteinortho_p_algorithm" : None,
        "gene_trees" : None,
        "species_tree" : None,
        "number_of_cc" : None,
        "edited_graphs" : None,
        "edition_error" : None,
        "unadmisible_triplets_trees" : None,
        "reconciliation" : None,
        "skipped" : None,
        "singletones_subset" : None,
        "_forestReconciliation" : None,
        "fastas_format" : None,
    }

    __project_options=["project_name",
                       "project_path",
                       "fastas_path",
                       "fastas_label",
                       "genes_dict",
                       "orthologs_list",
                       "orthology_graph",
                       "reported_edges",
                       "blast_run",
                       "proteinortho_steps",
                       "proteinortho_path",
                       "proteinortho_p_algorithm",
                       "gene_trees",
                       "species_tree",
                       "reset",
                       "create",
                       "fastas_format",
                      ]

    ################################
    #                              #
    # Project creation and storage #
    #                              #
    ################################

    def __init__(self, **kwargs):
        """
        Creates project directories
        and save configuration.
        """

        bad_options = set( kwargs ).difference(self.__project_options)
        if( len(bad_options) > 0 ):
            msg= "There are unknown options: {}. Type 'Project?' to see avalible options".format(", ".join(bad_options))
            errors.ProjectConfigurationError(msg)

        # Set some default options
        kwargs['create']= kwargs.get('create', False)
        kwargs['reset']= kwargs.get('reset', False)

        # Validate path and project name
        path= self.__validate_project_directory_path(kwargs)

        # Initialice project attributes
        self.__dict__.update(self.__project_attrs)
        self.__steps_dict={
                1 : self.__proteinortho,    # Orthology graph creation
                2 : self.__getEventTrees,    # Obtain genes tree
                3 : self.__reconciliate }   # reconciliate genes tree

        # Check if project exists
        if( self.__project_exists(path) ):
            self.__load_project(kwargs)
        else:
            if( kwargs['create'] ):
                self.__create_project(kwargs)
            else:
                msg= "There is not project '{}' in '{}' to load.".format( kwargs['project_name'] , kwargs['project_path'] )
                msg+= " If you want to create a new one set option 'create= True'"
                raise errors.ProjectDirectoryError(msg)

        # Verify attributes
        if(type(self.proteinortho_steps)==int ):
            self.proteinortho_steps= [ self.proteinortho_steps ]
        elif(type(self.proteinortho_steps)==str):
            if( "[","]" == self.proteinortho_steps[0],self.proteinortho_steps[-1] ):
                self.proteinortho_steps= [ int(i.strip()) for i in self.proteinortho_steps[1:-1].split(",") ]

    def __project_exists(self,path):
        return os.path.exists( path )

    def __create_project(self, kwargs):

        path= kwargs['path']
        os.makedirs(path)
        for directory in self.__project_dirs:
            os.makedirs( "{}{}/".format( path , directory ) )

        # Set default attributes
        kwargs["proteinortho_steps"]= kwargs.get("proteinortho_steps", 0 )
        kwargs["proteinortho_path"]= kwargs.get("proteinortho_path", "proteinortho6.pl" )

        # Save attributes
        self.__update_attributes( kwargs )
        
        self.number_of_cc= kwargs.get( "number_of_cc" ,0)
        self.edited_graphs= kwargs.get( "edited_graphs" ,dict())
        self.edition_error= kwargs.get( "edition_error" ,list())
        self.unadmisible_triplets_trees= kwargs.get( "unadmisible_triplets_trees" ,list())
        self.reconciliation= kwargs.get( "reconciliation" ,None)
        self._genes_dict= kwargs.get( "_genes_dict" ,None)
        self.skipped= kwargs.get( "skipped" ,list() )
        self.singletones_subset= kwargs.get( "singletones_subset" , list() )
        self._forestReconciliation= kwargs.get( "_forestReconciliation" , None )

        # Save project
        self.__save_project()

    def __save_project(self):
        """
        Write project attributes to Summary project sub.directory
        """
        with open( "{}Summary/project_attributes.txt".format(self.path) , "w" ) as F:
            F.write("project_name : {}\n".format(self.project_name))
            F.write("project_path : {}\n".format(self.project_path))
            F.write("path : {}\n".format(self.path))
            F.write("fastas_path : {}\n".format(self.fastas_path))
            F.write("fastas_label : {}\n".format(self.fastas_label))
            F.write("orthologs_list : {}\n".format(self.orthologs_list))
            F.write("blast_run : {}\n".format(self.blast_run))
            F.write("proteinortho_steps : {}\n".format(self.proteinortho_steps))
            F.write("species_tree : {}\n".format(self.species_tree))
            F.write("proteinortho_path : {}\n".format(self.proteinortho_path))
            F.write("number_of_cc : {}\n".format(self.number_of_cc))
            F.write("proteinortho_p_algorithm : {}\n".format(self.proteinortho_p_algorithm))
            F.write("orthology_graph : {}\n".format(self.orthology_graph))
            F.write("reconciliation : {}\n".format(self.reconciliation))

        with open("{}Summary/edited_graphs.txt".format(self.path) , "w" ) as F:
            if(len(self.edited_graphs)>0):
                lines= map( lambda i : "{}\t{}".format(i[0],i[1]) , self.edited_graphs.items() )
                F.write( "\n".join(lines)+"\n" )

        with open( "{}Summary/edition_error.txt".format(self.path) , "w" ) as F:
            if( len(self.edition_error)>0 ):
                F.write( "\n".join(self.edition_error) + "\n" )

        with open( "{}Summary/unadmisible_triplets_trees.txt".format(self.path) , "w" ) as F:
            if( len(self.unadmisible_triplets_trees)>0 ):
                F.write( "\n".join(self.unadmisible_triplets_trees) + "\n" )

        with open( "{}Summary/skipped_orthogroups.txt".format(self.path) , "w" ) as F:
            if(len(self.skipped)>0):
                F.write( "\n".join(map(str,self.skipped)) + "\n" )

        with open( "{}Summary/singletones_subset.txt".format(self.path) , "w" ) as F:
            if(len(self.singletones_subset)>0):
                F.write( "\n".join(map(str,self.singletones_subset)) + "\n" )

    def __load_project(self, kwargs ):
        """
        """
        # Check directories
        self.__check_subdirectorios(kwargs)

        # Load attributes
        str_2_object={"True" :True,
                      "False" : False,
                      "None" : None}

        read_object= lambda b : str_2_object.get(b, int(b) if b.isdigit() else b )

        with open("{}/Summary/project_attributes.txt".format(kwargs['path'])) as F:
            D= { a : read_object(b) for a, b in map( lambda i: i.strip().split(" : ") , F.readlines()) }
        self.__update_attributes( D )

        # Check if change attributes
        change={ a : b for a, b in kwargs.items() if a in self.__project_attrs and self.__dict__[a]!= b }
        if(len(change)>0):
            if( kwargs['reset'] ):
                self.__update_attributes( change )
            else:
                msg= "Tring to change project parameters: {}. If you want to proceed set the option 'reset=True'".format(" ,".join(change))
                raise errors.ProjectConfigurationError(msg)

        # Check table attributes
        change= dict()

        df= pd.read_csv("{}Summary/edited_graphs.txt".format(self.path), names= ("a","b") , sep="\t" )
        change[ "edited_graphs" ]= { a : b for _,(a,b) in df.iterrows() }

        with open( "{}Summary/edition_error.txt".format(self.path) ) as F:
            lines= map( str.strip , F.readlines() )
        change[ "edition_error" ]= [ int(s) if s.isdigit() else s for s in lines ]

        with open( "{}Summary/unadmisible_triplets_trees.txt".format(self.path) ) as F:
            lines= map( str.strip , F.readlines() )
        change[ "unadmisible_triplets_trees" ]= [ int(s) if s.isdigit() else s for s in lines ]

        with open( "{}Summary/skipped_orthogroups.txt".format(self.path)) as F:
            lines= map( str.strip , F.readlines() )
        change[ "skipped" ]= [ int(s) for s in lines ]

        with open( "{}Summary/singletones_subset.txt".format(self.path)) as F:
            lines= map( str.strip , F.readlines() )
        change[ "singletones_subset" ]= [ int(s) for s in lines ]

        self.__update_attributes( change )

    def save_as(self,**kwargs):

        kwargs["project_path"]= kwargs.get("project_path",os.path.abspath("./")+"/")
        kwargs["project_name"]= kwargs.get("project_name", "project")
        kwargs["path"]= "{}{}/".format(kwargs["project_path"],kwargs["project_name"])


        if( "force" in kwargs ):
            force= kwargs.pop("force")
        else:
            force= False

        if(self.__project_exists(kwargs["path"])):
            if(force):
                self.__update_attributes(kwargs)
                self.__save_project()
            else:
                msg=  "The project '{}' at '{}' already exists.".format(kwargs["project_name"], kwargs["project_path"])
                msg+= "You must change at least 'project_path' and/or 'project_name'  option."
                msg+= "Or you can set the option 'force=True' if you want to overwrite."
                raise errors.ProjectConfigurationError(msg)
        else:
            kw= dict(self.__dict__)
            kw.update(kwargs)
            self.__create_project( kw )

    def get_subSet(self, genes_filter, orthogroups_filter):
        self.skipped= list()
        identifiers= list()
        self.singletones_subset= list()
        CC= list()
        for ID, G in enumerate( map( self.getGraph , range(self.number_of_cc) ) ):
            if( orthogroups_filter( G , self.loadGenesdict() ) ):
                cc= list(filter( lambda gene : genes_filter(gene, self.loadGenesdict() ) , G.nodes() ))
                if(len(cc)>1):
                    CC+= [ G.subgraph(cc) ]
                    identifiers+= [ID]
                else:
                    self.singletones_subset+= [ID]
            else:
                self.skipped+= [ ID ]


        for ID, G in zip(identifiers, CC):
            nets.fprintAdjL(G,"{}".format(ID),self.path+"Graphs/",weighted=True)

        self.__update_attributes({"orthology_graph" : "{}Graphs/".format(self.path),
                                  "get_subSet" : True
                                 })

        self.__save_project()

    def __update_attributes(self, kwargs):
        D= {a : b for a, b in kwargs.items() if a in self.__project_attrs and b!=None}
        self.__dict__.update( D )

    def __validate_project_directory_path(self, kwargs):
        """
        Check if project_path directory exists, if project name is valid
        and returns the project direcotry path.
        """
        project_path= os.path.abspath(kwargs.get('project_path', "./" ))+"/"
        kwargs['project_path']= project_path
        project_name= kwargs.get('project_name', "project" )
        kwargs['project_name']= project_name
        kwargs['path']= "{}{}/".format(kwargs["project_path"], kwargs["project_name"])

        if( not os.path.exists( project_path ) ):
            msg= "the directory '{}' for 'project_path' option does not exists".format( project_path )
            raise errors.ProjectDirectoryError(msg)
        if( not os.path.isdir( project_path ) ):
            msg= "The option 'project_path' should be a directory instead of the regular file '{}'".format( project_path )
            raise errors.ProjectDirectoryError(msg)

        # Check project name characters
        not_allowed= set( project_name ).intersection( self.__not_allowed_characters )
        if( len(not_allowed) > 0 ):
            msg= "'project_name' option contans the not allowed characters: {}.".format(", ".join(not_allowed))
            raise errors.ProjectDirectoryError(msg)

        return "{}{}/".format( project_path , project_name )

    def __check_subdirectorios(self,kwargs):
        path= kwargs['path']
        if( not os.path.isdir(path) ):
                msg= "'{}' at '{}'. Project ({}) must be a directory".format(kwargs['project_name'],
                                                                              kwargs['project_path'],
                                                                              kwargs['project_name'])
                raise errors.ProjectDirectoryError(msg)

        for directory in self.__project_dirs:
            directory_path= "{}{}/".format( path , directory )
            if( os.path.exists( directory_path ) ):
                if( not os.path.isdir( directory_path ) ):
                    msg= "The regular file "
                    msg+= "'{}' at '{}'. Project ({}) "
                    msg+= "should be a directory.".format(directory,
                                                          path,
                                                          kwargs['project_name'])
                    raise errors.ProjectDirectoryError(msg)
            elif( kwargs['create'] ):
                os.makedirs( directory_path )
            else:
                msg= "There is no directory'{}' at '{}'. Set option 'create=True' to create it".format(directory,path)
                msg+=", but you may have lost analysis information."
                raise errors.ProjectDirectoryError(msg)

    ########################
    #                      #
    # Methods to load data #
    #                      #
    ########################

    # Genes data
    ############

    def loadGenesdict(self, reload= False):

        if( self._genes_dict != None and not reload ):
            return self._genes_dict

        D= dict()

        if( self.fastas_path==None or (self.fastas_format==None and self.fastas_label==None) ):
            if( self.genes_dict == None ):
                msg=  "You need to pass the genes_dict option or fastas_label and fastas_path. "
                msg+= "or fastas_path and fastas_format='ENSEMBLE'."
                msg+= "See file description at documentation."
                raise errors.ProjectConfigurationError(msg)
        else:

            fastas_label= FastasLabel(self.fastas_label , self.get_fastas_list(), self.fastas_format )

            kind_sequence= { "cdna" : "transcript" , "pep" : "protein" , "dna" : "gene" }

            D["species2genes"]= dict()
            D["gene2species"]= dict()
            D["fastaID_2_gene"]= dict()

            for file in self.get_fastas_list():

                file_s= file.split("/")[-1]
                species= fastas_label.file2label[ file_s ]
                kind= kind_sequence[ fastas_label.file2sequences_kind.get(file_s, "genes" ) ]

                species2kind= "species2{}s".format(kind)
                kind2species= "{}2species".format(kind)
                gene2kind= "gene2{}s".format(kind)
                kind2gene= "{}2gene".format(kind)

                D[species2kind]= D.get(species2kind,dict())
                D[species2kind][species]= D.get(species,set())
                D["species2genes"][species]= D.get(species,set())
                D[kind2species]= D.get(kind2species,dict())
                D[gene2kind]= D.get(gene2kind,dict())
                D[kind2gene]= D.get(kind2gene,dict())

                with open(file) as F:
                    lines= map(lambda line : line[1:].strip().split(" "),
                               filter(lambda line: line[0]==">",
                                      F.readlines()))

                for line in lines:
                    ID= line[0]
                    D[species2kind][species].add(ID)
                    D[kind2species][ID]= species

                    B= dict()
                    for item in line[2:]:
                        if(":" in item and not ( "[" in item or "]" in item ) ):
                            itsp= item.split(":")
                            field, value= itsp[0], ":".join(itsp[1:])
                            B[field]= value

                    gene= B["gene"]
                    if(gene== ""):
                        print(line)
                    D[gene2kind][gene]= D[gene2kind].get(gene,set())
                    D[gene2kind][gene].add(ID)
                    D[kind2gene][ID]= gene
                    D["fastaID_2_gene"][ID]= gene
                    D["species2genes"][species].add(gene)
                    D["gene2species"][gene]= species

                    for field, value in B.items():
                        if (field!="gene"):
                            D[field]= D.get(field,dict())
                            D[field][gene]= D[field].get( ID , set() )
                            D[field][gene].add(value)

            self._genes_dict= GenesDict( self.genes_dict , **D )

        return self._genes_dict


    # Trees data
    ############

    def getNewikcFileName(self,distinction):
        return "{}.newick".format(distinction)

    def getNewickFilePath( self , distinction ):
        return "{}Trees/{}".format(self.path, self.getNewikcFileName(distinction))

    def getNewickString( self, distinction ):
        with open( self.getNewickFilePath(distinction) ) as F:
            newick= F.readline().split(";")[0]
        return newick

    def getTree(self,distinction):
        if( distinction=="SPECIESTREE" ):
            t= evolutionaryTree.Tree(newick=basic_functions.readNewickFile(self.species_tree),type="species")
            t.cc= distinction
            return t
        else:
            newickStr= self.getNewickString( distinction )
            t= evolutionaryTree.Tree(newick= newickStr, type="genes", label2spice=self.loadGenesdict().gene2species)
            t.cc= distinction
            return t

    def get_valid_trees_ID( self ):
        avoidCCs= list(self.edited_graphs) + self.edition_error + self.skipped + self.singletones_subset
        filterCCs= lambda i : i not in avoidCCs
        treesID=list(chain(filter( filterCCs , range(self.number_of_cc) ),
                           chain.from_iterable((("{}_subCC-{}".format(i,j) for j in range(n))
                                                for i, n in self.edited_graphs.items()))))
        return treesID

    def get_valid_trees_list( self ):
        self.unadmisible_triplets_trees= list()
        treesID= self.get_valid_trees_ID()
        treesIterable=list()
        distinctions=list()
        for distinction in treesID:
            try:
                tree=self.getTree(distinction)
                treesIterable.append(tree)
                distinctions.append(distinction)
            except errors.tripletDefinitionError:
                self.unadmisible_triplets_trees.append(distinction)
            except: raise
        return treesIterable, distinctions

    # Orthology graphs

    def getAdjacencyListName(self,distinction,ext=""):
        return "{}{}".format(distinction,ext)

    def getGraph(self,distinction):

        adjl="{}{}".format(self.orthology_graph,
                           self.getAdjacencyListName(distinction,".adjL"))
        weights="{}{}".format(self.orthology_graph,
                              self.getAdjacencyListName(distinction,".weights"))

        return nets.Graph(adjacencyList= adjl,
                        adjacencyWeightsList= weights,
                        secuencesType= "genes",
                        identifierType= "genes",
                        genesdicts= self.loadGenesdict() ).G

    def get_unedited_graphs_identifiers(self):
        avoidCCs= self.skipped + self.singletones_subset
        return list(i for i in range(self.number_of_cc) if i not in avoidCCs)

    def get_unedited_graphs_list(self):
        IDs= self.get_unedited_graphs_identifiers()
        return IDs, [self.getGraph(ID) for ID in IDs]

    def get_edited_graphs_list(self):
        avoidCCs= self.edition_error + self.skipped

        IDs= list(chain.from_iterable((("{}_subCC-{}".format(i,j) for j in range(n))
                                   for i,n in self.edited_graphs.items()
                                   if i not in avoidCCs
                                  )))

        avoidCCs+= list(self.edited_graphs)
        IDs= [i for i in range(self.number_of_cc) if i not in avoidCCs] + IDs

        return IDs, [self.getGraph(ID) for ID in IDs]


    def get_fastas_list(self):
        return list( map( lambda i : self.fastas_path+i , filter( basic_functions.fasta_file_filter , os.listdir( self.fastas_path ) ) ) )

    def get_reconciliation(self, reload=True):

        if(self._forestReconciliation==None or reload):
            with open( self.reconciliation , "rb" ) as F:
                self._forestReconciliation= pickle.load( F )
        return self._forestReconciliation

    ###########################
    #                         #
    # Methods to run analysis #
    #                         #
    ###########################

    def __proteinortho( self ):
        """
        Calls proteinortho
        """

        fastas_list= self.get_fastas_list()

        for step in self.proteinortho_steps:
            proteinortho_command=[self.proteinortho_path,
                                  "-project={}".format( self.project_name ),
                                  "-verbose",
                                  "-graph",
                                  "-keep",
                                  "-singles",
                                  "-step={}".format(step),
                                  "-p={}".format(self.proteinortho_p_algorithm)] + fastas_list
            errorMsg= lambda e : "ERROR: proteinortho returned non-zero exit status 1: {}".format(e)

            print("RUNNING BASH COMMAND: ", " ".join( proteinortho_command ))
            basic_functions.executeBashCommand( proteinortho_command , errorMsg )

        self.__mv_prt_outputs()
        self.__update_attributes({"orthologs_list" : "{}Prt/{}.proteinortho-graph".format(self.path, self.project_name ),
                                  "__proteinortho" : True
                                 })

    def __mv_prt_outputs( self ):
        # Move prt files
        expected= [ "{}{}".format( self.project_name, extension) for extension in (".blast-graph",
                                                                                  ".proteinortho.html",
                                                                                  ".proteinortho.tsv",
                                                                                  ".proteinortho-graph",
                                                                                  ".proteinortho-graph.summary",
                                                                                  ".info")]
        filesList= set(os.listdir()).intersection( expected )
        outF= "{}.proteinortho-graph".format( self.project_name)
        if( outF not in filesList):
            raise errors.OutputError("There is not proteinortho output file {}. Do you moved or deleted it?".format(outF))
        self.__move_files( filesList , "{}Prt/".format(self.path) )

        # Mv blast files
        ext= self.__algorith_2_extension[ self.proteinortho_p_algorithm ]
        filesList= list( filter( lambda i : i.endswith(ext) , os.listdir() ) )
        if(len(filesList)==0):
            msg= "There are not output files of algorithm '{}'".format(self.proteinortho_p_algorithm)
            if( os.path.exists("proteinortho_cache_project/") ):
                filesList= list( map(lambda i : "proteinortho_cache_project/"+i,
                                     filter(lambda i : i.endswith(ext),
                                            os.listdir("proteinortho_cache_project/") )) )

                if(len(filesList)==0):
                    raise errors.OutputError(msg)
            else:
                raise errors.OutputError(msg)
        self.__move_files( filesList , "{}Blast/".format(self.path) )

    def __move_files( self, filesList, target_path ):
        for file in filesList:
            command=["mv",
                     file,
                     target_path]

            errorMsg= lambda e : "ERROR: Can not move algorithm output files: {}".format(e)
            basic_functions.executeBashCommand( command , errorMsg )

    def __edgesList_2_adjList( self ):
        """
        Transforms list of edges to a graph,
        saves it and it's connected components
        in format to run modular decomposition
        """
        # Transform edges list to networkx graph
        genesdict= self.loadGenesdict()

        if( self.orthologs_list.endswith(".proteinortho-graph") ):

            graph=nets.Graph(proteinOrthoGraph= self.orthologs_list,
                           orthologsListOrder= [1,2,4,6],
                           orthologsListAvoidHeaders= 4,
                           project= self.project_name,
                           projectPath= self.project_path,
                           secuencesType= "genes",
                           identifierType="genes",
                           genesdicts=genesdict)
            G=graph.G

        else:
            # revisar si es tsv
            raise NotImplementedError

        if( self.reported_edges!= None ):
            w= [e["weight"] for e in G.edges(data=True)]
            w_av= sum(w)/len(w)

            wf= lambda node: sum([e["weight"] for e in G.edges(node,data=True)])
            wef= lambda edge : wf(edge[0]) + wf(edge[1])

            edges=list()
            df= pd.read_csv(self.reported_edges,sep="\t")
            for _, e in df[["gen_a","gen_b"]].iterrows():
                w_ab= wef( e )
                if(w_ab==0):
                    w_ab= w_av
                edges+= [(e[0],e[1],w_ab0)]
            G.add_weighted_edges_from(edges)

        filePath= "{}{}/Graphs/".format( self.project_path, self.project_name )

        # Save graph
        nets.fprintAdjL(graph.G, self.project_name, filePath)

        # Obtain conected components and save
        file="{}Graphs/{}".format(self.path, self.project_name)
        adjacencyList= file+".adjL"
        adjacencyWeightsList= file+".weights"

        CC= basic_functions.conComp(G)
        self.number_of_cc= len(CC)
        nets.fprintCC(CC,
                      G,
                      self.project_name,
                      self.path+"Graphs/")

        self.__update_attributes({"orthology_graph" : "{}Graphs/".format(self.path),
                                  "__edgesList_2_adjList" : True
                                 })

    def __getEventTrees( self ):

        if(self.orthology_graph != None):
            pass
        elif( self.orthologs_list!= None ):
            self.__edgesList_2_adjList()
        else:
            msg=  "To run step 2 (create genes tree) you must pass orthology relations:\n"
            msg+= "  You can set the option 'orthologs_list' to pass '.tsv' or '.proteinortho-graph' file.\n"
            msg+= "  Or the 'orthology_graph' option to pass '.adjL' files.\n "
            msg+= "See description of files at the documentation by typing 'revolutiontl.Project.run_analysis?'"
            msg+= "or in the project repository: 'https://gitlab.com/jarr.tecn/revolutionh-tl'."
            raise errors.ProjectConfigurationError( msg )

        IDs= self.get_unedited_graphs_identifiers()

        drcc= nets.descomponerRCC(IDs,
                                  self.orthology_graph ,
                                  "{}Trees/".format(self.path) ,
                                  "genes",
                                  "genes",
                                  self.loadGenesdict(),
                                  editPrimesModules=True)

        self.edited_graphs= drcc.editedGraphs
        self.edition_error=drcc.editionError

        """
        drcc.saveEditions(self.path+"Summary/"+self.project_name+".editions")
        """
        drcc.saveSingletones(self.path+"Summary/"+self.project_name+".singletones")

        self.__update_attributes({"gene_trees" : "{}Trees/".format(self.path),
                                  "__getEventTrees" : True
                                 })

    def __reconciliate( self ):
        """
        reconciliate all the valid genes trees
        """

        if( self.gene_trees==None ):
            msg=  "Before run step 3 (trees reconciliation) you need to pass the "
            msg+= "'gene_trees' option or run step 2 (gene trees creation)."
            raise errors.ProjectConfigurationError( msg )

        if( self.species_tree==None ):
            # Crear árbol de especies con minCutSuperTree
            raise NotImplementedError(" By the moment it is needed a species tree to run step 3 (trees reconciliation) ")

        treesIterable, distinctions= self.get_valid_trees_list()
        speciesTree= self.getTree("SPECIESTREE")
        forestReconciliation=evolutionaryTree.ForestReconciliation(treesIterable,
                                                      speciesTree,
                                                      geneTreesID=distinctions,
                                                      display_warinigs=False)

        # Save reconciliation
        self._forestReconciliation= forestReconciliation
        self.reconciliation= "{}Reconciliation/forest_reconciliation.pickle".format(self.path)

        with open( self.reconciliation , "wb" ) as F:
            pickle.dump( forestReconciliation ,  F )

    def run_analysis( self, steps=0 ):
        """
        Run the analysis or specific steps.

        Inputs:

        - steps [int or list]:
          Specifies steps of analysis to run:
          One integer of { 0, 1, 2 ,3}
          or one sub-list of [1, 2, 3]

          If the integer is '0', then steps
          1,2 and 3 will be runed.

          The steps are:
          1. Create orthology graphs
          2. Create gene trees
          3. reconciliate gene trees
        """
        if( isinstance( steps , int ) ):
            if( steps == 0 ):
                steps=[ 1, 2, 3 ]
            elif( steps>0 and steps <=3 ):
                steps= [ steps ]
            else:
                raise ValueError("Steps must be a sub-list of [1,2,3]")
        elif( isinstance( steps , list ) ):
            if( len(steps) == 0 ):
                steps=[ 1, 2, 3 ]
            else:
                for step in steps:
                    if( step < 1 or step > 3 ):
                        raise ValueError("Steps must be a sub-list of [1,2,3]")
        else:
            msg= "ERROR: steps parameter for run_analysis must be an integer"
            msg+= " or a iterable of integers taken from [1, 2, 3]."
            raise ValueError(msg)
        for step in steps:
            node= self.__steps_dict[ step ].__name__

            self.__steps_dict[ step ]()
            self.__save_project()
