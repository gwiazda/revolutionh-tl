class ProjectDirectoryError(Exception):
    pass

class ProjectConfigurationError(Exception):
    pass

class CallError(Exception):
    pass

class OutputError(Exception):
    pass

class tripletDefinitionError(Exception):
    pass

# Errores por revisar

class reconciliationError(Exception):
    pass