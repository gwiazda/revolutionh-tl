import subprocess
import networkx as nx
from . import errors

def executeBashCommand( command, errorMesage=None):
    try:
        y=subprocess.check_call( command )
    except subprocess.CalledProcessError as e:
        if(errorMesage==None):
            errorMesage="Command '{}' returned non-zero exit status 1.".format(command)
        else:
            errorMesage= errorMesage( e )
        raise errors.CallError(errorMesage)
    except: raise

def fasta_file_filter( file_name ):
    file_name= file_name.lower()
    return file_name.endswith(".fa") or file_name.endswith(".fasta") or file_name.endswith(".faa")


def readNewickFile(file):
    """
    Gets the content of a fila containinf a newick file
    acoiding the ';'
    """
    with open(file) as F: newick=F.readline().split(";")[0]
    return newick

def getEdgesBetween(A,B,G,weighted=True,weight="weight"):
    """
    Generator of edges between subsets A and B of nodes of G
    Inputs:
    - A,B: Subset of nodes of G
    - G: Networkx greaph
    - weighted: bool
      - if True --> returns 3-tuples, where first 2 elements are nodes and third is weiht of edge
      - esle -----> returns 2-tuples
    - weight: label for weight of edges of G
    """
    A=set(A)
    B=set(B)
    if(weighted): getEdg=lambda a,b: (a,b,G[a][b][weight])
    else: getEdg=lambda a,b: (a,b)
    for a in A:
        for b in set(G.neighbors(a)).intersection(B):
            yield getEdg(a,b)

def conComp(G):
    """
    Regresa lista de listas
    Cada sublista contiene los nodos de cada componente cnexa
    """
    return ([list(com) for com in list(nx.connected_components(G))])