# -*- coding: utf-8 -*-
"""
Stoer-Wagner minimum cut algorithm.
Modified from networkx module:
- Now returns all the min cuts if returnAll=True
- stoer_wagner function splited in sub-functions
- partitions are frozenset instead of lists
Original code by ysitu <ysitu@users.noreply.github.com> from
	https://networkx.github.io/documentation/networkx-1.10/_modules/networkx/algorithms/connectivity/stoerwagner.html#stoer_wagner
	(29/06/2020)
Modification by jarr.tecn@gmail.com
"""

from itertools import islice
from . import errors
import networkx as nx
from networkx.utils import *

__all__ = ['stoer_wagner']

@not_implemented_for('directed')
@not_implemented_for('multigraph')

def stoer_wagner(Go, weight='weight', heap=BinaryHeap,returnAll=False):
	"""
	Returns the weighted minimum/maximum edge cut using the Stoer-Wagner algorithm.

	Determine the minimum/maximum edge cut of a connected graph using the
	Stoer-Wagner algorithm. In weighted cases, all weights must be
	nonnegative.

	The running time of the algorithm depends on the type of heaps used
	(described in the table below), also if returnAll=True, then the
	running time below must be multiplied by the number of nodes in G

	============== =============================================
	Type of heap   Running time
	============== =============================================
	Binary heap	`O(n (m + n) \log n)`
	Fibonacci heap `O(nm + n^2 \log n)`
	Pairing heap   `O(2^{2 \sqrt{\log \log n}} nm + n^2 \log n)`
	============== =============================================

	Parameters
	----------
	G : NetworkX graph
		Edges of the graph are expected to have an attribute named by the
		weight parameter below. If this attribute is not present, the edge is
		considered to have unit weight.

	weight : string
		Name of the weight attribute of the edges. If the attribute is not
		present, unit weight is assumed. Default value: 'weight'.

	heap : class
		Type of heap to be used in the algorithm. It should be a subclass of
		:class:`MinHeap` or implement a compatible interface.

		If a stock heap implementation is to be used, :class:`BinaryHeap` is
		recommeded over :class:`PairingHeap` for Python implementations without
		optimized attribute accesses (e.g., CPython) despite a slower
		asymptotic running time. For Python implementations with optimized
		attribute accesses (e.g., PyPy), :class:`PairingHeap` provides better
		performance. Default value: :class:`BinaryHeap`.

	returnAll: bool

	Returns
	-------
	cut_value : integer or float
		The sum of weights of edges in a minimum cut.

	partition :
		if returnAll=False then partition is a simple partitoon
		else partition is a set of partitions

	Raises
	------
	NetworkXNotImplemented
		If the graph is directed or a multigraph.

	NetworkXError
		If the graph has less than two nodes, is not connected or has a
		negative-weighted edge.

	errors.notPreciseResult
		If there was fount two diferent cut_values for two diferent initializacion nodes (when returnAll=True)
	"""
	n=checkGraphFeatures(Go,weight)
	allPartitions=set()
	allCutValues=list()
	#golbalCutVal=float("inf")
	#init_cut_value=golbalCutVal
	if(returnAll): initNodes=Go.nodes()
	else: initNodes=[Go.nodes()[0]]
	# Run minCut with diferent initial node (a) for min cut phase. (if all the min cuts are desired)
	for a in initNodes:
		G=getGraphCopy(Go,weight)
		cut_value, partitions =minCut(G,a,n,heap=heap)
		allPartitions.update(partitions)
		allCutValues.append(cut_value)
		#golbalCutVal=updateBestPartitions(allPartitions,partitions,golbalCutVal,init_cut_value,cut_value)
	if returnAll:
		return cut_value, allPartitions
	else:
		return cut_value, partitions.pop()

"""
def updateBestPartitions(allPartitions,partitions,golbalCutVal,init_cut_value,cut_value):
	allPartitions.update(partitions)
	if(golbalCutVal==init_cut_value): golbalCutVal=cut_value
	elif(golbalCutVal!=cut_value):raise errors.notPreciseResult("There was found more than one min cut value for diferent start nodes: {} vs {}".format(golbalCutVal, cut_value))
	return golbalCutVal
"""


def checkGraphFeatures(G,weight):
	"""
	- Inputs:
	  - G: Networkx Graph (no digraf nor multigraph)
	  - weight: label which identifies the weight of the edges

	- Outputs:
	  - Copy of graph.
	  - number of nodes.
	  - raises error if the graph has not the apropiated characteristics.

	- Process:
	  - Makes a copy of the graph for internal use.
	  - Checks:
	    - The number of nodes in the graph.
	    - The weight of the edges.
	"""
	n = len(G)
	if n < 2:
		raise nx.NetworkXError('graph has less than two nodes.')
	if not nx.is_connected(G):
		raise nx.NetworkXError('graph is not connected.')
	# Make a copy of the graph for internal use.
	for u, v, e, in G.edges_iter(data=True):
		if e[weight] < 0:
			raise nx.NetworkXError('graph has a negative-weighted edge.')
	return n

def getGraphCopy(G,weight):
	return nx.Graph((u, v, {'weight': e.get(weight, 1)})
				for u, v, e in G.edges_iter(data=True) if u != v)

def minCut(G,a,n,heap=BinaryHeap):
	cut_value=float('inf')
	best_phases=[]
	nodes = set(G)
	contractions = []  # contracted node pairs
	for i in range(n - 1):
		A, u, v, w = minCutPhase(G,a,i,n,heap=heap)
		if(w<cut_value):
			cut_value=w
			best_phases=[i]
		elif(w==cut_value):
			best_phases+=[i]
		contractST(G,u,v,contractions)
	return cut_value, recoverOptimalPartitions(best_phases,contractions,nodes)

def minCutPhase(G,a,i,n,heap=BinaryHeap):
	# Pick an arbitrary node u and create a set A = {u}.
	u=a
	A = set([u])
	# Repeatedly pick the node "most tightly connected" to A and add it to
	# A. The tightness of connectivity of a node not in A is defined by the
	# of edges connecting it to nodes in A.
	h = heap()  # min-heap emulating a max-heap
	for v, e in G[u].items():
		h.insert(v, -e['weight'])
	# Repeat until all but one node has been added to A.
	for j in range(n - i - 2):
		u = h.pop()[0]
		A.add(u)
		for v, e, in G[u].items():
			if v not in A:
				h.insert(v, h.get(v, 0) - e['weight'])
	v, w = h.min()
	return A, u, v, -w

def contractST(G,u,v,contractions):
	# Contract v and the last node added to A.
	contractions.append((u, v))
	for w, e in G[v].items():
		if w != u:
			if w not in G[u]:
				G.add_edge(u, w, weight=e['weight'])
			else:
				G[u][w]['weight'] += e['weight']
	G.remove_node(v)

def recoverOptimalPartitions(best_phases,contractions,nodes):
	# Recover the optimal partitioning from the contractions.
	Gs=list()
	Vs=list()
	partitions=set()
	for best_phase in best_phases:
		G=nx.Graph(islice(contractions, best_phase))
		v = contractions[best_phase][1]
		G.add_node(v)
		Gs.append(G)
		Vs.append(v)
		reachable = set(nx.single_source_shortest_path_length(G, v))
		partition = frozenset((frozenset(reachable), frozenset(nodes - reachable)))
		partitions.add(partition)
	return partitions