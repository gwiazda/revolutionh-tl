# coding=utf-8
import networkx as nx
import pandas as pd
import os
from copy import deepcopy
from . import order
from itertools import combinations,chain
from . import errors
from . import basic_functions

from . import evolutionaryTree as evo
from . import stoerwagner
import sys
from tqdm import tqdm

rootPath="/".join(os.path.realpath(__file__).split("/")[:-1])+"/"

def edgesWeightFunction(Dict):
    if(Dict==None):
        weightFun=lambda nodeC,node: 1
    else:
        weightVer=lambda nodeC,node: (node,nodeC) in Dict
        weightFun=lambda nodeC,node: Dict[(nodeC,node)] if weightVer(nodeC,node) else 10
    return weightFun

def decomposeGraph(adjacencyList,newick):
    #command= "java -jar {}.iced-md.jar {} {}".format(rootPath,adjacencyList,newick)
    command=["java",
             "-jar",
             "{}.iced-md.jar".format(rootPath),
             adjacencyList,
             newick
            ]
    basic_functions.executeBashCommand(command)
"""
# Dictionary (secuenceType,identifierType) ----> 
secuence_nodes_configuration={
    
}
"""
class Graph:

    def __init__(self,
                 G=None,
                 adjacencyList=None,
                 adjacencyWeightsList=None,
                 proteinOrthoGraph=None,
                 orthologsListOrder=(1,2,3,4),
                 orthologsListAvoidHeaders=0,
                 project=None,projectPath=None,
                 secuencesType=None,
                 identifierType=None,
                 genesdicts=None):
        self.initAtributes(G,adjacencyList,adjacencyWeightsList,project,projectPath,secuencesType,identifierType,proteinOrthoGraph,orthologsListOrder,orthologsListAvoidHeaders,genesdicts)

    def initAtributes(self,G,adjacencyList,adjacencyWeightsList,project,projectPath,secuencesType,identifierType,proteinOrthoGraph,orthologsListOrder,orthologsListAvoidHeaders,genesdicts):
        self.genesdicts=genesdicts
        if(G==None): self.G=nx.Graph() #evoGraph
        else: self.G=G
        self.__initPaths(project,projectPath)
        self.__initNodeMethods(identifierType,secuencesType)
        self.adjacencyList=adjacencyList
        self.adjacencyWeightsList=adjacencyWeightsList
        if(adjacencyList!=None):
            self.add_adjacencyList(adjacencyList,genesdicts,adjacencyWeightsList=adjacencyWeightsList)
        self.proteinOrthoGraph=proteinOrthoGraph
        if(proteinOrthoGraph!=None):
            self.add_proteinOrthoGraph(proteinOrthoGraph,identifierType,orthologsListOrder,orthologsListAvoidHeaders)

    def __initPaths(self,project,projectPath):
        if(project==None): self.project=""
        else: self.project=project
        if(projectPath==None): self.projectPath="./"
        else: self.projectPath=projectPath
        self.path=self.projectPath+self.project+"/"

    def __initNodeMethods(self,identifierType,secuencesType):
        self.identifierType=identifierType
        self.secuencesType=secuencesType

    def add_adjacencyList(self,filePath,genesdicts,adjacencyWeightsList=None):
        if(self.identifierType=="genes"):
            if(self.secuencesType=="genes"): self.edgesss=lambda line:self.getEdgesAdjL(line)
            elif(self.secuencesType=="proteins"): self.edgesss=lambda line: self.getProtein2GenEdgesAdjL(line)
            else: raise ValueError("no valid secuencesType")
        elif(self.identifierType=="proteins"):
            if(self.secuencesType=="genes"): raise ValueError("nets.add_adjL cant get genes identifierType from proteins secuencesType")            
            elif(self.secuencesType=="proteins"): self.edgesss=lambda line:self.getEdgesAdjL(line)
            else: raise ValueError("no secuencesType valid")
        else: raise ValueError("no secuencesType valid")
        with open(filePath) as F:
            if(adjacencyWeightsList==None):
                self.readAdjFile(F)
            else:
                with open(adjacencyWeightsList) as F1:
                    self.readAdjFiles(F,F1)
        for node in self.G.nodes():
            self.G.node[node]["type"]=self.genesdicts.gene2species[node]

    def readAdjFile(self,F):
        for line in F:
            line=line.split("\n")[0]
            edges=self.edgesss(line)
            self.G.add_edges_from(edges)

    def readAdjFiles(self,F,F1):
        for line,line1 in zip(F,F1):
            line=line.split("\n")[0]
            line1=line1.split("\n")[0]
            edges=self.edgesss(line)
            edges1=self.getEdgesAdjL(line1)
            for A,B in zip(edges,edges1):
                a,b=A
                _,wab=B
                self.G.add_edge(a,b,weight=float(wab))

    def add_proteinOrthoGraph(self,proteinOrthoGraph,identifierType,orthologsListOrder,orthologsListAvoidHeaders):
        if(type(orthologsListOrder)==str):
            orthologsListOrder=[int(i)-1 for i in orthologsListOrder.split(",")]
        if(len(orthologsListOrder)!=4):
            raise errors.ReadEdgesListError("The orthologsListOrder parameter must be a list of integers, example: '1,2,3,4' instead of '{}'".format(orthologsListOrder))
        with open(proteinOrthoGraph) as F:
            edges=dict()
            for i,line in enumerate(F.readlines()):
                if(line[0]!="#"):
                    namea, nameb, bits_ave= self.getProteinEdgeProteinortho(line,orthologsListOrder,orthologsListAvoidHeaders)
                    edge=frozenset(map(lambda name: self.genesdicts.fastaID_2_gene[name],
                                       (namea, nameb)))
                    bits_sum, ocurrences = edges.get(edge,(0,0))
                    edges[edge]=bits_sum+bits_ave, ocurrences+1
            self.G.add_weighted_edges_from([(namea, nameb, bits_ave/occurrences) for ((namea, nameb), (bits_ave, occurrences)) in edges.items()])
        for node in self.G.nodes():
            self.G.node[node]["type"]=self.genesdicts.gene2species[node]

    def getProteinEdgeProteinortho(self,line,orthologsListOrder,orthologsListAvoidHeaders):
        #prota,protb,eval_ab,bits_ab,eval_ba,bits_ba=line.split()
        orthologsListOrder= [i-1 for i in orthologsListOrder]
        ls=line.split()
        prota,protb,bits_ab,bits_ba=ls[orthologsListOrder[0]],ls[orthologsListOrder[1]],ls[orthologsListOrder[2]],ls[orthologsListOrder[3]]
        #eval_ab,eval_ba=map(float,(eval_ab,eval_ba))
        bits_ab,bits_ba=map(float,(bits_ab,bits_ba))
        #eval_ave=(eval_ab+eval_ba)/2
        #bits_ave=(bits_ab+bits_ba)/2
        bits_ave=(bits_ab+bits_ba)/(2*max(bits_ab,bits_ba))
        return prota,protb,bits_ave

    """
    def getGenEdgeProteinortho(self,line,genesdicts):
        prota,protb,eval_ab,bits_ab,eval_ba,bits_ba=line.split()
        gena,genb=genesdicts.protein2gen[prota],genesdicts.protein2gen[protb]
        eval_ab,eval_ba=float(eval_ab),float(eval_ba)
        return gena,genb,(eval_ab+eval_ba)/2
    """

    def getEdgesAdjL(self,line):
        nodea,nodesb=line.split(" | ")
        nodesb=nodesb.split(",")
        edges=[(nodea,nodeb) for nodeb in nodesb]
        return edges

    def getProtein2GenEdgesAdjL(self,line):
        nodea,nodesb=line.split(" | ")
        gena=self.genesdicts.protein2gen[nodea]
        nodesb=nodesb.split(",")
        genesb=[]
        edges=[]
        for nodeb in nodesb:
            genb=self.genesdicts.protein2gen[nodeb]
            if(genb not in genesb):
                genesb+=[genb]
                edges+=[tuple([gena,genb])]
        return edges

def netName(G,dif=None,weight=False):    # Genera nombre de red
    name="N"+str(len(G.nodes()))
    if(dif!=None): name+="."+str(dif)
    if(weight): name+=".weighted"
    else: name+=".no_weighted"
    return(name)

def fprintCC(CC,
             G,
             baseName,
             filesPath,
             weighted=True,
             identifiers=None):    # Imprime CC de G en un directorio name dentro de path

    if( identifiers==None ):
        identifiers= range(len(CC))

    for i, ID in enumerate(identifiers):
        fprintAdjL(G.subgraph(CC[i]),"{}".format(ID),filesPath,weighted)

class fprintAdjL:
    def __init__(self,graph,fileName,filePath,weighted=True):
        """
        Imprime lista de adjacencia con formato para descomposició modular con el programa iced
        """
        self.G=graph
        pathName= "{}{}.adjL".format(filePath, fileName)
        with open(pathName,"w") as Fg:
            pathName1=  "{}{}.weights".format(filePath, fileName)
            with open( pathName1 ,"w") as Fw:
                for node in self.G.nodes(): self.printNode(node,Fg,Fw,weighted)

    def printNode(self,node,Fg,Fw,weighted=True):
        """
        - Fg is the file where the adjacencies will be saved
        - Fw is where the weights of the edges will be saved
        """
        Fg.write(str(node)+" | ")
        Fw.write(str(node)+" | ")
        edges=self.G.edges(node,data=True)
        if(len(edges)>0):
            for edge in edges[:-1]:
                Fg.write("{},".format(edge[1]))
                Fw.write("{},".format(edge[2]["weight"]))
            Fg.write("{}\n".format(edges[-1][1]))
            Fw.write("{}\n".format(edges[-1][2]["weight"]))

def getMaxModularPartitionChildrenOfPrimeModules(tp):
    primeNodes=[]
    bfs=order.Tour(tp.root)
    bfs.actions=[lambda node: primeNodes.append(node)]
    bfs.condition=lambda node: node.type=="*"
    bfs.go()
    partitions=[]
    for node in primeNodes[::-1]:
        partitions.append([[leaf.label for leaf in son.inducedLeaves] for son in node.soons])
        #partitions.append([son.inducedLeavesType for son in node.soons])
    return partitions

def isPartition(S,P):
    """
    returns True if P is a partition of S
    inputs:
    - S: set
    - P: list of sets
    """
    ret=True
    N=set()
    for part in P:
        if(not N.isdisjoint(part)):
            ret=False
            break
        N.update(part)
    if(ret and len(S.difference(N))>0):
        ret=False
    return ret

def getWeightOfEdgesBetween(A,B,G,weight="weight"):
    """
    Returns the sum of the edges between subsets A and B of nodes of G
    """
    AB_weiht=0
    for edge in basic_functions.getEdgesBetween(A,B,G,weighted=True,weight=weight):
        AB_weiht+=edge[2]
    return AB_weiht

def getQuiotentGraph(G,P,weight="weight"):
    """
    Returns the weighted quotient graph H of a graph G given
    a partiton P of it's nodes.

    In:
    - G: Networkx graph
    - P: List of set of nodes such that the unioin is V(G) and the intersection is void.
    - weight: atribute of edges taken as a weight
    """
    # Check partition
    if(not isPartition(set(G.nodes()),P)):
        raise errors.QuiotentGraphError("Nodes of graph are not the disjoint union of parts of P")
    # Make quiotent graph
    G_p=nx.Graph()
    nodes=[frozenset(part) for part in P]
    G_p.add_nodes_from(nodes)
    for A,B in combinations(nodes,2):
        w=getWeightOfEdgesBetween(A,B,G,weight=weight)
        if(w>0): G_p.add_edge(A,B,attr_dict={weight:w})
#    G_p.add_weighted_edges_from([(A,B,getWeightOfEdgesBetween(A,B,G,weight=weight)) for A,B in combinations(nodes,2)])
    return G_p

def colorByPartition(G,partition,partition_id):
    partition="partition_{}_part".format(partition_id)
    involvedNodes=list(chain.from_iterable(partition))
    labels={part:str(i) for i,part in enumerate(partition)}
    for node in G.nodes():
        if(node not in involvedNodes):
            G.node[node][partition]="None"
        else:
            for part in partition:
                if(node in part):
                    G.node[node][partition]=labels[part]

def popAnyKey(D):
    key=list(D)[0]
    return key, D.pop(key)

def editPrimeGraph(Go,to,treeType,editionsOutputName="editedGraph",graphsOutputPath="./",treesOutputPath="./"):
    """
    - Given the modular decomposition tree tp of the graph Gp:
    - get the prime modules of Gp begining from the smaller ones. For each one:
      - get the coarset modules smaller than the prime module, this set of subset of nodes is named maxMP.
      - get the sub-graph G_ of G induced by the union of elements of 'maxMP'
      - get the quiotent graph G_maxMP
      - computes minCut of G_maxMP
      - removes the edges of G involved in the cut of G_maxMP
    """
    G_total_edited=Go.copy()
    editEdges=list()
    graphCC2moduTree=[(Go.copy(), to)]
    cc_number=0
    singletones=list()
    while(len(graphCC2moduTree)>0):
        Gpp,tp=graphCC2moduTree.pop(0) #popAnyKey(graphCC2moduTree)
        Mps=getMaxModularPartitionChildrenOfPrimeModules(tp)
        if(len(Mps)>0):
            maxMP=Mps[-1]
            G_maxMP=Gpp.subgraph(chain.from_iterable(maxMP)).copy()
            G_maxMP=getQuiotentGraph(G_maxMP,maxMP)
            CC=basic_functions.conComp(G_maxMP)
            if(len(CC)>1):
                raise errors.UnexpectedError("Quotient graph is not conex")
            cutw,CutPartitions=stoerwagner.stoer_wagner(G_maxMP,returnAll=True)
            cutSets=chooseBestCutSet(CutPartitions,Gpp)
            editionFiles=set()
            delta=0
            """
            for edges in (set(chain.from_iterable(combination))
                            for i in range(1,len(cutSets)+1) 
                            for combination in combinations(cutSets, i)):
            """
            for edges in cutSets:
                # Remove edges of the cutSet
                Gp=Gpp.copy()
                Gp.remove_edges_from(edges)
                isPrime=False
                # Remove files of the try with past cutSet
                for file in editionFiles:
                    os.remove(file)
                editionFiles=set()
                cc_number-= delta
                delta=0
                # Checks CC of editedGraph
                Gp_components=list() #dict()
                singles=list()
                for cc in basic_functions.conComp(Gp):
                    Gcc=Gp.subgraph(cc)
                    newick="{}{}_subCC-{}.newick".format(treesOutputPath,editionsOutputName,cc_number)
                    adjl="{}_subCC-{}".format(editionsOutputName,cc_number)
                    if(len(cc)>1):
                        fprintAdjL(Gcc,adjl,graphsOutputPath,weighted=True)
                        adjl=graphsOutputPath+adjl
                        decomposeGraph(adjl+".adjL",newick)
                        nwkT=basic_functions.readNewickFile(newick)
                        no_nodes=[nn for nn in Gcc.nodes() if nn not in nwkT]
                        if( len(no_nodes)>0 ):
                            msg="Modular decomposition tree miss the node(s): {}".format(", ".join(no_nodes))
                            raise UnexpectedError(msg)
                        prime= "*" in nwkT
                        isPrime=isPrime or prime
                        if(prime):
                            Gp_components.append((Gcc.copy(), evo.Tree(newick=nwkT,type=treeType,weighted=False,displayWarning=False)))
                            map(os.remove,(newick,adjl+".adjL",adjl+".weights") )
                        else:
                            cc_number+=1
                            delta+=1
                            editionFiles.add(newick)
                            editionFiles.add(adjl+".adjL")
                            editionFiles.add(adjl+".weights")
                            nx.write_gexf(Gcc, adjl+".gexf")                        
                            editionFiles.add(adjl+".gexf")                        
                    else:
                        singles.append(cc[0])
                if(not isPrime):
                    break
            for e0, e1 in edges:
                w=G_total_edited.get_edge_data(e0, e1)
                w["cut_status"]= w.get("cut_status","")+ "_cuted"
            editEdges+=edges
            singletones+=singles
            if(isPrime):
                graphCC2moduTree.extend(Gp_components) #.update(Gp_components)
    N_of_cc=cc_number+len(singletones)
    return G_total_edited,editEdges,singletones,N_of_cc

def chooseBestCutSet(CutPartitions,Gp):
    """
    Returns a list of cutSets ordered by cardanlity.
    """
    cutSets=list()
    for As,Bs in CutPartitions:
        edgess=list(basic_functions.getEdgesBetween(chain.from_iterable(As),chain.from_iterable(Bs),Gp,weighted=False))
        cutSets.append(edgess)
    cutSets=sorted( cutSets , key= lambda cutSet : len(cutSet) )
    return cutSets

def descomponer2( IDs_list, in_graphs_path ,out_trees_path, genesdict , projectName= "" ):

    editions= dict()
    singletones= dict()
    editedGraphs= dict()
    editionError= list()

    for ID in tqdm(IDs_list, desc= "Decomposing graphs" ):

        adjacencyList= "{}{}-cc{}.adjL".format( in_graphs_path , projectName, ID )
        adjacencyWeightsList= "{}{}-cc{}.weights".format( in_graphs_path , projectName, ID )
        newick = "{}{}-cc{}.newick".format( out_trees_path , projectName, ID )
        decomposeGraph( adjacencyList , newick )

        nwkT= basic_functions.readNewickFile(newick)
        if("*" in nwkT):
            t= evo.Tree(newick= nwkT, type="genes" ,weighted=False, displayWarning=False)
            G= Graph(
                    adjacencyList= adjacencyList,
                    adjacencyWeightsList= adjacencyWeightsList,
                    secuencesType= "genes",
                    identifierType= "genes",
                    genesdicts= genesdict).G
            editionsOutputName= "{}-cc{}".format( projectName, ID )
            try:
                G_ed, editEdges, singletones_d, N_of_cc=editPrimeGraph(G,
                                                                     t,
                                                                     treeType= "genes",
                                                                     editionsOutputName= editionsOutputName,
                                                                     graphsOutputPath= in_graphs_path,
                                                                     treesOutputPath= out_trees_path)
            except (errors.notPreciseResult,errors.UnexpectedError):
                editionError.append( ID )
            except:
                raise
            else:
                editions[ID]={"removed":editEdges,"added":list()}
                singletones[ID]=singletones_d
                editedGraphs[ID]=N_of_cc-len(singletones_d)
    return editions, singletones, editedGraphs, editionError


class descomponerRCC:
    def __init__(self,
                 identifiers,
                 adjacencyListsPath,
                 treesPath,
                 secuencesType,
                 identifierType,
                 genesdict,
                 edgesWeight=lambda a,b:1,
                 editPrimesModules=False):
        
        self.adjacencyListsPath= adjacencyListsPath
        self.treesPath= treesPath
        self.editPrimesModules=editPrimesModules
        self.secuencesType=secuencesType
        self.identifierType=identifierType
        self.genesdict=genesdict
        self.toEdit=[]
        self.editions=dict()
        self.singletones=dict()
        self.editedGraphs=dict()
        self.editionError=list()

        for (adjacencyList,newick),distinction in tqdm(map(self.getAdjAndNewickPath,
                                                           identifiers ),
                                                       total=len(identifiers),
                                                       desc= "Decomposing graphs" ):
            G=self.getGraph(distinction)
            n_nodes=len(G.nodes())
            decomposeGraph(adjacencyList,newick)
            nwkT=basic_functions.readNewickFile(newick)
            if("*" in nwkT):
                t=self.getTree(distinction)
                #G,t=self.getGraphAndTree(distinction)
                editionsOutputName=self.getAdjacencyListName(distinction)
                try:
                    G_ed,editEdges,singletones,N_of_cc=editPrimeGraph(G,t,
                                                        treeType=self.secuencesType,
                                                        editionsOutputName=editionsOutputName,
                                                        graphsOutputPath=self.adjacencyListsPath,
                                                        treesOutputPath=self.treesPath)
                except (errors.notPreciseResult,errors.UnexpectedError):
                    self.editionError.append(distinction)
                except: raise
                else:
                    self.editions[distinction]={"removed":editEdges,"added":list()}
                    self.singletones[distinction]=singletones
                    self.editedGraphs[distinction]=N_of_cc-len(singletones)

    def saveEditions(self, file ):
        with open(file,"w") as F:
            for distinction,editions in self.editions.items():
                if(len(editions["added"])+len(editions["removed"])>0):
                    F.write("> {}\n".format(distinction))
                    F.write("- added\n")
                    for edge in chain.from_iterable(editions["added"]): F.write("{}\n".format(edge))
                    F.write("- removed\n")
                    #for edge in chain.from_iterable(editions["removed"]): F.write("{}\n".format(edge))
                    for edge in editions["removed"]: F.write("{}\t{}\n".format(edge[0],edge[1]))

    def saveSingletones(self, file):
        with open(file,"w") as F:
            for distinction,singletones in self.singletones.items():
                if(len(singletones)>0):
                    F.write("> {}\n{}\n".format(distinction,"\n".join(singletones)))

    def getAdjAndNewickPath(self,distinction):
        adjacencyList=self.getAdjacencyListPath(distinction)
        newick=self.getNewickPath(distinction)
        return (adjacencyList,newick),distinction

    def getGraphAndTree(self,distinction):
        G=self.getGraph(distinction)
        t=self.getTree(distinction)
        return G,t

    def getGraph(self,distinction):
        adjacencyList=self.getAdjacencyListPath(distinction)
        adjacencyWeightsList=self.getAdjacencyWeightsListPath(distinction)
        return Graph(adjacencyList=adjacencyList,adjacencyWeightsList=adjacencyWeightsList,secuencesType=self.secuencesType,identifierType=self.secuencesType,\
                genesdicts=self.genesdict).G

    def getTree(self,distinction,newick=None):
        if(newick==None): newick=basic_functions.readNewickFile(self.getNewickPath(distinction))
        return evo.Tree(newick=newick,type=self.secuencesType,weighted=False,displayWarning=False)

    def getAdjacencyListPath(self,distinction):
        return "{}{}".format(self.adjacencyListsPath,self.getAdjacencyListName(distinction))+".adjL"

    def getAdjacencyWeightsListPath(self,distinction):
        return "{}{}".format(self.adjacencyListsPath,self.getAdjacencyListName(distinction))+".weights"

    def getNewickPath(self,distinction):
        return "{}{}.newick".format(self.treesPath,distinction)

    def getAdjacencyListName(self,distinction):
        #return "{}.{}-cc{}".format(self.project,self.identifierType,distinction)
        return "{}".format(distinction)

    def saveDescomposition(self,newick,distinction):
        with open(newick) as F: newick=F.readline()
        if("*" in newick):
            self.toEdit+=[distinction]        