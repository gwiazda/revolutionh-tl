# coding=utf-8
from .  import order
import os
from itertools import combinations,permutations,product,chain
#import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
from copy import deepcopy
from . import errors
from . import basic_functions
from . import stoerwagner
import warnings
from tqdm import tqdm

# Define equivalence of tree types:

speciesTreeTypes=["species",None]
sequencesTreeTypes=["genes","proteins"]

# Define funcions

def repeatANtimes(A,N):
    for i in range(N): yield A

def takeOneVsTheRest(itemsList):
    """
    for each element E of the itemsList
    yields a tuple with the element and
    the itemsList without E
    """
    for i in range(len(itemsList)):
        yield (itemsList[i],itemsList[:i]+itemsList[i+1:])

def inducedLeafTypesUnion(nodesList):
    """
    rturns a sorted tuple containing the union
    of leaf types induced by the nodes in
    nodesList
    """
    ret=set()
    for node in nodesList: ret.update(node.inducedLeavesType)
    return tuple(sorted(ret))

def areCongruent(t,T):
    """
    returns tuple ( bool , list )

    - bool : True if species tree T displays all triplets of genes tree t
    - list : List of incongruent triplets of t with T.
    """
    incongruents,congruents=incongruentTriplets(t,T)
    congruent=len(incongruents)==0
    return congruent,incongruents,congruents

def incongruentTriplets(t,T):
    """
     Returns the triplets in t
        that makes t not to be a sub tree of T
     Buscar Toda tripleta de t en T:
        primero buscar outgrou: por cada elemento
        luego ingroupp: por cada par
    """
    incongruets=[]
    congruents=[]
    for triplet in t.getTripletBag():
        if(triplet in T.getTripletBag()):
            congruents.append(triplet)
        else:
            incongruets.append(triplet)
    return incongruets,congruents

def incongruentBranchs(t,T):
    """
    returns list of nodes of t that induces
    an tree not congruent with T
    """
    bfs_t=order.Tour(t.root)
    incBran=[]
    bfs.actions=[lambda n:chechCongruencyAndFixBfs(n,T,incBran,bfs)]
    bfs.go()
    return incBran

def chechCongruencyAndFixBfs(n_t,T,incBran,bfs):
    """
    - checks if triplets are congruent with tree T
    - If don not are congruent:
        - for each n in the sons of n_t that produces incongruency:
            - n will be appended to incBran
            - n will not visited by bfs
    """
    switchedUp,switchedDown=getIncongruencies(n_t,T)
    for badGroup in switchedUp,switchedDown:
        n_son_bad=n_t.tree.typesLastCommonAncestor(badgroup)
        incBran.append(n_son_bad)
        bfs.quitNodeFromItems(n_son_bad)

def appendIfIntersection(setA,setB,apList):
    inter=(setA.intersection(setB))
    if(len(inter)>0): apList.append(tuple(sorted(inter)))

def tripletSetsList(n):
    return [(A,B) for A in n.tripletSets for B in n.tripletSets[A]]

def getOutGroupsOfNodeThatIntersectsOutG(outG,node):
    """
    returns the dictionary of the tripletsets
    of the node that contains outG
    """
    ret=[]
    outG=set(outG)
    for outG_ts in node.tripletSets:
        if(outG.issubset(outG_ts)):
            ret.append(outG_ts)
    return ret

def getIncongruencies(n_t,T):
    """
    - returns the incongruencies of the triplets of n_t with the tree T
    - there are two tipes of incongruency, so the function returns two
      lists of nodes:
        - switchedUp: branches of n_t that are outGroups, but in T should be inGroup
        - switchedDown: branches of n_t that are inGroup, but in T should be outGroups
    """
    n_T=T.typesLastCommonAncestor(n_t.inducedLeavesType)
    switchedUp=[]
    switchedDown=[]
    for out_t,in_t in tripletSetsList(n_T):
        it=set(in_t)
        found=set()
        for o_t in out_t:
            out_T=getOutGroupsOfNodeThatIntersectsOutG(o_t,n_T)
            for o_T in out_T:
                inter=it.intersection(n_T.tripletSets[o_T])
                if(len(inter)>1):
                    found.update(inter)
            


    for A_t,B_t in tripletSetsList(n_t):
        At=set(A_t)
        Bt=set(B_t)
        for A_t in getOutGroupsOfNodeThatIntersectsOutG(A_t,n_T):
            
            appendIfIntersection(Bt,A_T,switchedDown)
            for B_T in n_T.tripletSets[A_T]: appendIfIntersection(At,B_T,switchedUp)
    return switchedUp,switchedDown

def getVoidList():
    return list()

def wasCorrectMap(tree,returnIncongruents=False):
    incongruents=[]
    bfs=order.Tour(tree.root)
    bfs.actions=[lambda node: [None if node.weight+node.singletones+node.edgesGain[soon]+node.edgesGain_singletones[soon]-node.edgesLose[soon]==soon.weight\
                    else incongruents.append(soon)\
                    for soon in node.soons]]
    bfs.go()
    correct=len(incongruents)==0
    if(returnIncongruents): return correct,incongruents
    else: return correct

def getLeavOrthologs(leav):
    orthologs=[]
    bfs=order.Tour(leav.tree.root,kind="BFS")
    bfs.actions=[lambda node: [None if leav.isNodeInPath(soon)\
                    else [orthologs.append(indLeave) for indLeave in soon.inducedLeaves]\
                    for soon in node.soons]]
    bfs.condition=lambda node: node.type=="S"
    bfs.restriction=lambda node: not leav.isNodeInPath(node)
    bfs.go()
    return orthologs

def LeavsTypeUnion(trees):
    """
    Return the union of the leaves in a trees list
    """
    S=set()
    for t in trees:
        for l in t.type2leaves:
            S.add(l)
    return list(S)

def getTripletsAsTrees(tree):
    trees=[]
    tripletLeaf2treeLeaf=dict()
    bfs=order.Tour(tree.root,kind="BFS",actions=[lambda n: nodeTriplets2Trees(n,tripletLeaf2treeLeaf=tripletLeaf2treeLeaf,trees=trees) ])
    bfs.go()
    return trees, tripletLeaf2treeLeaf

def nodeTriplets2Trees(node,tripletLeaf2treeLeaf=dict(),trees=list()):
    for a,(b,c) in node.triplets:
        datingMap=node.inGroup2datingMap[frozenset((b,c))]
        aa,bb,cc=map(lambda node: Node(label=node.label,type=node.type), (a,b,c))
        map(lambda n,nn: tripletLeaf2treeLeaf.update({nn:n}), ((a,aa),(b,bb),(c,cc)) )
        T_out= Tree(root=aa, type=node.tree.type,label2spice=node.tree.label2spice)
        T_in=starTree([bb,cc],nodeType=datingMap,treetype=node.tree.type,label2spice=node.tree.label2spice)
        trees+=[joinTrees([T_out,T_in],treetype=node.tree.type)]
    return trees, tripletLeaf2treeLeaf

def matchesA2B(A,B):
    """
    A is a list/tuple/set
    B is a list/tuple/set of lists/tuples/sets
    returns dictinary { subset of A : [sublist 1 of B,sublist 2 of B, ... ] }
    """
    matchs={}
    A=set(A)
    for b in B:
        inte=A.intersection(b)
        if(len(inte)>0):
            inte=tuple(sorted(inte))
            if(inte in matchs): matchs[inte]+=[b]
            else: matchs[inte]=[b]
    return matchs

class treeCopier:

    def __init__(self,tree,restriction=lambda node: False,root=None,validateTriplets=True):
        self.tree=tree
        self.tree2ctree={}
        self.restriction=restriction
        if(root==None): root=self.tree.root
        self.ctree=Tree(type=tree.type,label2spice=tree.label2spice,weighted=tree.weighted,validateTriplets=validateTriplets)
        posOrder=order.Tour(root,kind="DFS",DFSOrder="pos",actions=[self.getNodeCopy],restriction=restriction)
        posOrder.go()
        self.ctree.addNode(node=self.tree2ctree[root])
        #self.ctree.fixLeaves(self.ctree.root)
        self.ctree.root.setInducedLeavesAndTriplets(validateTriplets=validateTriplets)
        self.ctree.root.setAge()

    def getNodeCopy(self,node):
        nodeCopy=Node(label=node.label,tree=self.ctree,path=node.path[:],type=node.type,weight=node.weight,singletones=node.singletones)
        if(node.edgesGain==None):
            copyNodeAtr=lambda dictAtr,dictKey,dictAtrCp,dictKeyCp: None
        else:
            nodeCopy.edgesGain=dict()
            nodeCopy.edgesLose=dict()
            nodeCopy.edgesGain_singletones=dict()
            copyNodeAtr=lambda dictAtr,dictKey,dictAtrCp,dictKeyCp: dictAtrCp.update({dictKeyCp : dictAtr[dictKey]})
        for soon in node.soons:
            if(not self.restriction(soon)):
                copySoon=self.tree2ctree[soon]
                nodeCopy.addSoon(node=copySoon)
                copyNodeAtr(node.edgesGain,soon,nodeCopy.edgesGain,copySoon)
                copyNodeAtr(node.edgesLose,soon,nodeCopy.edgesLose,copySoon)
                copyNodeAtr(node.edgesGain_singletones,soon,nodeCopy.edgesGain_singletones,copySoon)
        self.tree2ctree[node]=nodeCopy

class Tree:

    treeTypes=speciesTreeTypes+sequencesTreeTypes  #secuenceTypes+speciestype

    def __init__(self,newick=None,label=None,root=None,createRoot=False,type=None,\
        label2spice=None,type2leaves=None,weighted=False,validateTriplets=True,displayWarning=True):
        self.initAtributes(label,root,createRoot,type,type2leaves,weighted,label2spice,validateTriplets,displayWarning)
        if(newick!=None):
            NewickReader(newick,tree=self,label2spice=label2spice,weighted=weighted)
            self.root.setAge()
        self.setDiameter()

    def setDiameter(self):
        self.diameter=0
        for leaf in self.leavs:
            l=len(leaf.path)
            if(l>self.diameter): self.diameter=l

    def __repr__(self):
        return self.root.__repr__()

    def nodesLastCommonAncestor(self,L):
        L=list(L)
        if(len(L)>1):
            maxPathLen=max([len(node.path) for node in L])
            f=False
            for i in range(maxPathLen):
                pathElements=[node.path[i] if len(node.path)>i else None for node in L]
                if(None in pathElements or False in [pathElement==pathElements[0] for pathElement in pathElements]):
                    f=True
                    break
            if(f):
                return self.nodeOfPath(L[0].path[:i])
            else: return L[0]
        elif(len(L)==1):
            return L[0]

    def typesLastCommonAncestor(self,L):
        """
        Returns the last comon ancestor of
        an iterable of leafs type
        """
        leaves=[]
        for Leavetype in L: leaves+=self.type2leaves[Leavetype]
        return self.nodesLastCommonAncestor(leaves)

    def labelsLastCommonAncestor(self,L):
        """
        Returns the las common ancestir of
        an iterable of leafs label
        """
        leafs=[]
        for label in L:
            found=False
            for leaf in self.leavs:
                if(leaf.label==label):
                    found=True
                    leafs.append(leaf)
                    break
            if(not found):
                raise ValueError("There is no leaf in the tree with label '{}'".format(label))
        return self.nodesLastCommonAncestor(leafs)

    def nodeOfPath(self,path):
        node=self.root
        for i in path:
            node=node.soons[i]
        return node

    def performTreeTypeDependentFunctions(self,functions,treeTypeSuperList,mandatory=None):
        """ 
        Performs the i-th function onf functions list
        if the tree type is in the i-th list of treeTypeSuperList.
        Mandatory determines when raise an error if certain functions where not runned

        input:
        functions: list of functions [f_1, f_2, f_3,...]
        treeTypes: list of lists of treeTypes [ [tt_1_1, tt_1_2,...] , [tt_2_1,tt_2_2,...] , [...] ... ]
        mandatory: list of bools
            An error is raised if the i-th element of mandatory
            is True and the i-th method were not runned
        """
        ret=[]
        if(mandatory==None):
            mandatory=repeatANtimes(False,len(functions))
            flag=True
        elif(mandatory==True):
            mandatory=repeatANtimes(False,len(functions))
            flag=False
        for treeTypeList,isMand in zip(treeTypeSuperList,mandatory):
            if(self.type in treeTypeList):
                ret.append(function())
                flag=True
            elif(isMand): raise errors.invalidTreeType("Method {} can only be performed on tree of types: {}".format(function,treeTypes))
        if(flag): return ret
        else: raise errors.invalidTreeType("Method {} can only be performed on tree of types: {}".format(function,treeTypesSuperList))

    def initAtributes(self,
                      label,
                      root,
                      createRoot,
                      type,
                      type2leaves,
                      weighted,
                      label2spice,
                      validateTriplets,
                      displayWarning):

        if(type in self.treeTypes): self.type=type
        else: raise errors.invalidTreeType("Unknown tree type '{}', only avalible:'{}' ".format(type,self.treeTypes))
        self.tripletBag=None
        self.label=label
        self.leavs=[]
        self.superTrees=[]
        self.root=None
        self.weighted=weighted
        self.validateTriplets=validateTriplets
        if(label2spice==None):
            self.label2spice=dict()
        else:
            self.label2spice=label2spice
        if(label2spice==None and type in ["genes","proteins"] and displayWarning):
            warnings.warn("Tree type is '{}', but label2spice dictionary is not provided so the species of leafs will be the same as it's label.".format(type))
        if(type2leaves==None): self.type2leaves={}
        else: self.type2leaves=type2leaves
        if(root==None):
            if(createRoot):
                self.addNode()
                self.root.setAge()
                self.root.setInducedLeavesAndTriplets(validateTriplets=self.validateTriplets)
        else:
            self.addNode(node=root)
            self.root.setInducedLeavesAndTriplets(validateTriplets=self.validateTriplets)
            self.root.setAge()


    def addNode(self,node=None,dad=None,label=None,soons=[]):
        """
         Adds node soon of dad
         if no dad --> soon of root
             if no root --> creates it is the new root
        """
        if(node==None): node=Node(label=label,dad=dad)
        node.tree=self
        if(dad==None):
            if(self.root==None):
                self.root=node
                self.fixPathsAndTreeOwner(node,node_path=[],node_tree=self)
                dad=self.root
            else:
                self.root.addSoon(node=node)
                self.fixPathsAndTreeOwner(node,node_path=node.path,node_tree=self)
                dad=self.root
        else:
            dad.addSoon(node=node)
            self.fixPathsAndTreeOwner(node,node_path=node.path,node_tree=self)
        self.fixLeaves(dad)

    def fixPathsAndTreeOwner(self,node,node_path=[],node_tree=None):
        node.path=node_path
        node.tree=node_tree
        for i,son in enumerate(node.soons):
            self.fixPathsAndTreeOwner(son,node_path=node_path+[i],node_tree=node_tree)

    def quitBranch(self,node,collapseIfOnlyOneSonLeft=True):
        dad=node.dad
        if(dad==None):
            self.root=None
            self.leavs=[]
        else:
            idx=node.path[-1]
            dad.quitSon(node,idx)
            for son in dad.soons[idx:]: son.fixPath()
            for leaf in node.inducedLeaves: self.deConcatenateLeaf(leaf)
            L=len(dad.soons)
            if(L==0): self.concatenateLeaf(dad)
            elif(L==1 and collapseIfOnlyOneSonLeft):
                nDad=dad.soons[0]
                self.collapseNode(dad,setTripletAndIndLeafs=False)
                dad=nDad
            self.root.setInducedLeavesAndTriplets(restriction=lambda node: not dad.isNodeInPath(node),validateTriplets=self.validateTriplets)

    def collapseNode(self,node,setTripletAndIndLeafs=True):
        if(node.dad!=None and node.dad.edgesGain!=None):
            el=node.dad.edgesLose.pop(node)
            eg=node.dad.edgesGain.pop(node)
            egs=node.dad.edgesGain_singletones.pop(node)
        else:
            el,eg=0,0
        self.quitBranch(node,collapseIfOnlyOneSonLeft=False)
        for son in node.soons:
            self.addNode(node=son,dad=node.dad)
        if(node.edgesGain!=None):
            for son in node.soons:
                node.dad.edgesLose[son]=node.edgesLose[son]+el
                node.dad.edgesGain[son]=node.edgesGain[son]+eg
                node.dad.edgesGain_singletones[son]=node.edgesGain_singletones[son]+egs
        if(setTripletAndIndLeafs): self.root.setInducedLeavesAndTriplets(restriction=lambda n: not node.dad.isNodeInPath(n),
                                                                        validateTriplets=self.validateTriplets)

    def fixNodeLeaf(self,node):
        if(node.isLeaf()): self.concatenateLeaf(node)
        else: self.deConcatenateLeaf(node)

    def fixLeaves(self,node):
        bfs=order.Tour(node,kind="BFS",actions=[self.fixNodeLeaf])
        bfs.go()

    def concatenateLeaf(self,node):
        if(node not in self.leavs): self.leavs+=[node]

    def deConcatenateLeaf(self,node):
        f=False
        for i in range(len(self.leavs)):
            if(node==self.leavs[i]):
                f=True
                break
        if(f): del self.leavs[i]

    def getTripletBag(self):
        if(self.tripletBag==None):
            self.tripletBag={}
            bfs=order.Tour(self.root,kind="BFS",actions=[self.addTripletSets2bag])
            bfs.go()
        return self.tripletBag

    def addTripletSets2bag(self,node):
        for outGroup in node.tripletSets:
            for outItem,inList in product(outGroup,node.tripletSets[outGroup]):
                for inItem0,inItem1 in combinations(inList,2):
                    tripleta=(outItem,frozenset((inItem0,inItem1)))
#                    if(tripleta in self.tripletBag):
#                        raise errors.AchisAchis("Se supone que los conjuntos de tripletas solo contienen una vez a cada tripleta en todos los nodos")
#                    else: self.tripletBag[tripleta]=0
                    self.tripletBag[tripleta]=0
                    #else: self.tripletBag[tripleta]=1

    def load(getFileName,name=None,dif=None,weight=False,type=None,label2spice=None):
        filePath,fileName=getFileName("Trees/","newick",name=name,dif=dif)
        newickFile=filePath+fileName
        newick=basicFunctions.readNewickFile(newickFile)
        return Tree(newick=newick,type=type,label2spice=label2spice)

    def save(self,getFileName,name=None,dif=None,weight=False,autoCorrectDif=False):
        filePath,fileName=getFileName("Trees/","newick",name="{}.{}".format(name,self.type),dif=dif,autoCorrectDif=autoCorrectDif)
        if(fileName in os.listdir(filePath)):
            raise errors.existingObject("Tree {} alrredy exists".format(filePath+fileName))
        else:
            with open(filePath+fileName,"w") as F:
                F.write(self.root.labelAsSuperNode(type=type,weight=weight)+";")

    def getFileName(self,project,path,name=None,dif=None,weight=False,autoCorrectDif=False):
        filePath="{}{}/Summary/".format(path,project)
        if(dif==None):
            dif=""
            difInt=1
        else: dif="-{}".format(dif)
        if(name==None): name=project
        fileName="{}.{}{}.newick".format(name,self.type,dif)
        if(autoCorrectDif):
            files=os.listdir(filePath)
            while(fileName in files):
                dif="-{}".format(difInt)
                difInt+=1
                fileName="{}.{}{}.newick".format(name,self.type,dif)
        return filePath,fileName

def getNodesInducedLeafType(S0):
    return tuple(sorted(set([leafType for S0i in S0 for leafType in S0i.inducedLeavesType])))

class Node:

    idCounter=0

    def __init__(self,label=None,soons=None,dad=None,tree=None,type=None,path=None,\
            weight=None,edgesGain=None,edgesLose=None,age=None,singletones=None,edgesGain_singletones=None):
        # soons is a list of nodes
        if(soons==None): self.soons=[]
        else: self.soons=soons
        # dad is a node
        if(label==None):
            self.label=Node.idCounter
            Node.idCounter+=1
        else: self.label=label
        self.setTypeAndDependentMethods(type)
        self.dad=dad
        self.tree=tree
        self.path=path
        self.weight=weight    # int
        self.singletones= singletones
        self.edgesGain=edgesGain    # { soon : int}
        self.edgesLose=edgesLose    # { soon : int}
        self.edgesGain_singletones=edgesGain_singletones
        self.age=age
        self.tripletSets={}
        self.subTripletSets={}

    def setTypeAndDependentMethods(self,type):
        self.type=type
        if(type in ["S",None]): self.getSpetiationSons=self.getSons
        elif(type=="P"): self.getSpetiationSons=self.getSonsOfSons
        else: self.getSpetiationSons=getVoidList

    def getSons(self):
        return self.soons

    def getSonsOfSons(self):
        return [sson for son in self.soons for sson in son.soons]

    def __repr__(self):
        return "{} {}".format(self.label,self.soons if len(self.soons)>0 else "")

    def addSoon(self,node=None):
        if(node==None): node=Node()
        node.dad=self
        self.soons+=[node]
        node.fixPath()
        if(None not in [self.weight,self.edgesLose]):
            self.edgesLose[node]=0
            self.edgesGain[node]=0
            self.edgesGain_singletones[node]=0

    def fixPath(self):
        if(self.dad.path==None): raise ValueError("Node dad has no path")
        else:
            idx=self.dad.soons.index(self)
            self.path=self.dad.path+[idx]
            for son in self.soons: son.fixPath()
            self.setAge()

    def quitSon(self,son,idx=None):
        if(idx==None): idx=self.soons.index(son)
        return self.soons.pop(idx)

    def addTripletSet(self,outSet,inSet):
        inter=set(outSet).intersection(inSet)
        if(len(inter)>0):
            raise errors.tripletDefinitionError("outSet and in Set intersection must be void set instead of [{}]".format(inter))
        elif(len(outSet)==0):
            raise errors.tripletDefinitionError("outSet must have at least 1 element")
        elif(len(inSet)<2):
            raise errors.tripletDefinitionError("inSet must have at least 2 elements")
        else:
            if(outSet in self.tripletSets): raise errors.existingObject("Trying to asign an already existing outSet for dict of triplet set")
            self.tripletSets[outSet]=[inSet]

    def addSubTripletSet(self,node):
        if(node.inducedLeavesType in self.subTripletSets): self.subTripletSets[node.inducedLeavesType]+=[node]
        else: self.subTripletSets[node.inducedLeavesType]=[node]

    def setTripletSets(self):
        """
        creates the dictionary self.tripletSets
        where keys are outSets and it's avlues
        are the respective inSets.
        """
        self.tripletSets={}
        if(self.type in ["S",None]):
            for S1,S0 in takeOneVsTheRest(self.soons):
                if(not S1.isLeaf() and len(S1.inducedLeavesType)>1 and len(S0)>0):
                    self.addTripletSet(getNodesInducedLeafType(S0),S1.inducedLeavesType)
                if(len(S1.inducedLeavesType)>2):
                    self.addSubTripletSet(S1)

    def getNodeTriplets(self,validateTriplets=True):
        self.triplets=list()
        self.inGroup2datingMap=dict()
        if(self.type in ["S",None]):
            for son_a,son_b in combinations(self.soons,2):
                self.__checkNodesInducedLeafsIntersection(son_a,son_b,validateTriplets)
                self.__addTripletSons(son_a,son_b)
                self.__addTripletSons(son_b,son_a)

    def __addTripletSons(self,in_son,out_son):
        for leaf_out in out_son.inducedLeaves:
            for leaf_a, leaf_b in combinations(in_son.inducedLeaves,2):
                inGroup=frozenset((leaf_a, leaf_b))
                self.triplets+=[(leaf_out, inGroup)]
                self.inGroup2datingMap[inGroup]=in_son.type

                """
                for type_a, type_b in combinations(type2leafs, 2):
                    for leaf_a, leaf_b in product(type2leafs[type_a], type2leafs[type_b]):
                        inGroup=frozenset((leaf_a, leaf_b))
                        self.triplets+=[(leaf_out, inGroup)]
                        self.inGroup2datingMap[inGroup]=in_son.type
                """

    def __checkNodesInducedLeafsIntersection(self,node_a,node_b,validateTriplets):
        if(not set(node_a.inducedLeavesType).isdisjoint(node_b.inducedLeavesType) and validateTriplets):
            raise errors.tripletDefinitionError("There must be no intersection in the induced leafs of the sons of a node type '{}'. The intersection is the set ({})".format(self.type,set(node_a.inducedLeavesType).intersection(node_b.inducedLeavesType)))

    def setInsets(self):
        self.inSets=dict()
        for A,B in combinations(self.soons,2):
            for a,b in product(A.inducedLeavesType,B.inducedLeavesType):
                if(a!=b):
                    inSet=frozenset((a,b))
                    if(inSet in self.inSets): self.inSets[inSet]+=1
                    else: self.inSets[inSet]=1

    def isLeaf(self):
        return len(self.soons)==0

    def labelAsSuperNode(self,type=False,weight=False):
        if(self.isLeaf()):
            if(weight): return "{}:{}[{}]_{}[{}]_{}".format(
                                                            self.label,
                                                            self.weight,
                                                            self.singletones,
                                                            self.dad.edgesGain[self],
                                                            self.dad.edgesGain_singletones[self],
                                                            self.dad.edgesLose[self])
            else: return str(self.label)
        else:
            ret="("
            for soon in self.soons[:-1]: ret+=soon.labelAsSuperNode(type=type,weight=weight)+","
            ret+=self.soons[-1].labelAsSuperNode(type=type,weight=weight)+")"
            if(type): ret+=str(self.type)
            if(weight):
                if(self.dad==None): ret+=":{}[{}]_{}[{}]_{}".format(
                                                                self.weight,
                                                                self.singletones,
                                                                self.tree.root_duplications, 0, 0 )
                else:
                    ret+=":{}[{}]_{}[{}]_{}".format(
                                                    self.weight,
                                                    self.singletones,
                                                    self.dad.edgesGain[self],
                                                    self.dad.edgesGain_singletones[self],
                                                    self.dad.edgesLose[self])
            return ret

    def setInducedLeavesAndTriplets(self,restriction=lambda node:False,validateTriplets=True):
        if(not restriction(self)):
            if(self.isLeaf()):
                self.inducedLeavesType=tuple([self.type])
                self.inducedLeaves=tuple([self])
                self.tree.type2leaves[self.type]= self.tree.type2leaves.get(self.type, list())+ [self]
                """
                if(self.type in self.tree.type2leaves): self.tree.type2leaves[self.type]+=[self]
                else: self.tree.type2leaves[self.type]=[self]
                """
                self.setInsets()
            else:
                soonInducedLeavesType=set()
                soonInducedLeaves=set()
                for soon in self.soons: #getSpetiationSons():
                    soon.setInducedLeavesAndTriplets(restriction=restriction,validateTriplets=validateTriplets)
                    soonInducedLeavesType.update(soon.inducedLeavesType)
                    soonInducedLeaves.update(soon.inducedLeaves)
                self.inducedLeavesType=tuple(sorted(soonInducedLeavesType))
                soonInducedLeaves=list(soonInducedLeaves)
                self.inducedLeaves=tuple([soonInducedLeaves[i] for i in np.argsort([leave.label for leave in soonInducedLeaves])])
                self.setInsets()
                self.setTripletSets()
            self.getNodeTriplets(validateTriplets=validateTriplets)

    def sumGene(self):
        self.weight+=1

    def loseGene(self):
        self.dad.edgesLose[self]+=1

    def duplicateGene(self,duplications):
        self.dad.edgesGain[self]+=duplications

    def isNodeInPath(self,node):
        if(len(self.path)>=len(node.path)):
            l=len(node.path)
            return node.path==self.path[:l]
        else: return False

    def setAge(self):
        self.age=len(self.path)

class NewickReader:

    indicators={")":None,"(":None,",":None}

    def __init__(self,newick,tree=None,root=None,treeType=None,label2spice=None,weighted=True):
        self.initAtributes(newick,tree,root,treeType,label2spice,weighted)
        # La string se va leyendo de derecha a izquierda
        # Con forme va leyendo genera árbol
        # Se construye como un dfs
        self.readItem()
        self.createItem()
        while(self.pointer<self.newickLen):
            self.readItem()
            self.createItem()
        self.tree.root.setInducedLeavesAndTriplets(validateTriplets=self.tree.validateTriplets)

    def createItem(self):
        if(self.pointerElement==")"):    # Create a node, go to node
            node=self.createNode(self)
            self.tree.addNode(node=node,dad=self.actualNode)
            self.setDad2nodeWeights(node)
            self.actualNode=node
        elif(self.pointerElement=="("):    # create leaf if avalibre, set leaf and actual node induced leavs ,set node triplets ,retuen to dad
            if(self.item!=""):
                node=self.createNode(self)
                node.type=self.label2spice(node.label)
                self.tree.addNode(node=node,dad=self.actualNode)
                self.setDad2nodeWeights(node)
            self.actualNode=self.actualNode.dad
        elif(self.pointerElement==","):    # create leaf if avalibre, set leaf induced leavs
            if(self.item!=""):
                node=self.createNode(self)
                node.type=self.label2spice(node.label)
                self.tree.addNode(node=node,dad=self.actualNode)
                self.setDad2nodeWeights(node)

    def setDad2nodeWeights(self,node):
        if(len(self.dad2nodeWeights)==3):
            node.dad.edgesGain[node]=self.dad2nodeWeights[0]
            node.dad.edgesGain_singletones[node]= self.dad2nodeWeights[1]
            node.dad.edgesLose[node]=self.dad2nodeWeights[2]

    def typeNoWehgtDecomposeItem(self,node):
        if(self.item!=""):
            node.type=self.item
            node.label=self.item
        self.dad2nodeWeights=[]

    def typeWehgtDecomposeItem(self,node):
        if(self.item==""):
            node.type=None
            self.dad2nodeWeights=[]
        else:
            type,weights=self.item.split(":")
            if(len(type)>0):
                node.type=type
                node.label=type
            self.getNewikcWeight(weights,node)

    def labelNoWehgtDecomposeItem(self,node):
        if(self.item!=""): node.label=self.item
        self.dad2nodeWeights=[]

    def labelWehgtDecomposeItem(self,node):
        if(self.item==""):
            node.label=None
            node.weight=None
            node.singletones=None
            self.dad2nodeWeights=[]
        else:
            label,weights=self.item.split(":")
            if(len(label)>0): node.label=label
            self.getNewikcWeight(weights,node)

    def getNewikcWeight(self,weights,node):
        weights=weights.split("_")
        weights=[weights[0], weights[1].split("[")[0], weights[1].split("[")[0][:-1], weights[2] ]
        node.weight=int(weights[0].split("[")[0])
        node.singletones=int(weights[0].split("[")[1][:-1])
        if(len(weights)>1): self.dad2nodeWeights=[int(weights[1]),int(weights[2]),int(weights[3])]
        else: self.dad2nodeWeights=[]


    def item2event(self):
        node=Node()
        if(self.weighted): self.typeWehgtDecomposeItem(node)
        else: self.typeNoWehgtDecomposeItem(node)
        return node

    def item2label(self):
        node=Node(type="S",weight=0,edgesGain={},edgesLose={},singletones=0,edgesGain_singletones={})
        if(self.weighted): self.labelWehgtDecomposeItem(node)
        else: self.labelNoWehgtDecomposeItem(node)
        return node

    def itemIgnore(self):
        self.dad2nodeWeights=[]
        return Node()

    def readItem(self):
        self.item=""
        self.getPointerElement()
        while((self.pointerElement not in NewickReader.indicators)&(self.pointer<self.newickLen)):
            self.getPointerElement()
        self.item=self.item[::-1][1:]

    def getPointerElement(self):
        self.pointerElement=self.newick[self.newickLen-1-self.pointer]
        self.item+=self.pointerElement
        self.pointer+=1

    def initAtributes(self,newick,tree,root,treeType,label2spice,weighted):
        self.initTree(tree,root)
        self.actualNode=self.root
        self.initItemInterpretation()
        self.newick=newick
        self.newickLen=len(newick)
        self.pointer=0
        self.item=""
        self.pointerElement=""
        if(label2spice==None): self.label2spice=lambda label:label
        else: self.label2spice=lambda label:label2spice[label]
        self.weighted=weighted

    def initTree(self,tree,root):
        if(tree==None):
            if(root==None):
                self.tree=Tree(createRoot=False,type=treeType)
                self.root=self.tree.root
            else:
                self.tree=root.tree
                self.root=root
        else:
            self.tree=tree
            if(root==None): self.root=tree.root
            else: self.root=root

    def initItemInterpretation(self):
        if(self.tree.type=="species"): self.createNode=lambda reader: reader.item2label()
        elif(self.tree.type=="genes"): self.createNode=lambda reader: reader.item2event()
        elif(self.tree.type=="proteins"): self.createNode=lambda reader: reader.item2event()
        elif(self.tree.type==None): self.createNode=lambda reader: reader.itemIgnore()
        else: raise AttributeError("tree type '{}' not avalible\nuse 'species', 'genes' or 'proteins'".format(self.fastasFormat))

class Recon:
    """
    This class maps the nodes of genes tree t to
    nodes and edges of species tree T and save it
    in attributes.

    Inputs:
    -------

    - t
      [Tree] such that t.type="genes"

    - T
      [Tree] such that T.type="species"

    - errorWhenNotCongruent
      [Bool]

    Atributtes:
    -----------

      The map is saved in the attributes listed below.
      For a detailed description continue reading.

      For nodes N in species tree T:

      - N.weight
      - N.edgesGain
      - N.edgesLose
      - N.singletones

      For species tree T:

      - T.root_duplications

      For genes tree t:

      - t.map

      For nodes n in t:
      - n.M

    Notation:
    ---------

    Trees:

    - t
      genes tree

    - T
      species tree

    - n
      an arbitrary but fixed node of t

    - N
      an arbitrary but fixed node of T

    - E
      an arbitrary but fixed edge of T

    - @
      an arbitrary but fixed node of an arbitrary but fixed tree

    Attributes and operations on Nodes:

    - @.D
      Dad of @

    - @ < @'
      @' is ancestor of @'
      or equivalently
      @ is descendent of @'
      
    - @ <> @'
      not ( @ < @' or @' < @ or @ = @')

    - T >= (@, @', @'', ...)
      Last common ancestors of @, @', @'', ... in the tree T

    - T <= @
      Induced sub tree of T by @

    Equivalences

    
    not @'''< @''  <==>  @'' <= @''' or @'' <> @'''

    Symbols:

    - Sy
      one symbol of
      - .r
        Gene inheritance: Resolved by species tree spetiation event
      - xK
        loss of gene

    Map description:
    ----------------

    Each node n of t is mapped to an element
    M of T that is a node N or edge E:
    n -> M <- {N,E}

    The type of M deppends on the type of n:
    - if n.S = "S" then M = N
    - if n.S = "P" then M = E

    an edge E is a tuple of dad an son nodes (@,@') such that
    @'.D = @
    So we can also denote an edge by the node soon
    E := @'.De
    Then the map may be done alwas to a node of T
    as long as it is distinguished when the
    mapping corresponds to a node and when to an edge:
    n -> M <- {N,N.De}

    To determine which node N of T is the correspondig
    to the map n -> M <- {N,N.De} we select the last
    common ancestor on T of the leaf types of the tree
    induced by n:
    N = l.T< n.inducedLeavs

    The map n -> M implies that it must have existed at least
    one gene in ancestral or existing specie N of T:
    - if n is a leaf
      then a single  gene will be mapped  to a leaf N
    - if n.S = "S"
      then a single gen is mapped to N
    - if n.S = "P"
      then x-1 duplications are mapped to N.De
      and x genes are mapped to N, where
      x is the number of soons of n.

    Note that when we say that _a gene is mapped to N_, we
    are saying that during the map n -> M <- {N,N.De}
    it was mapped to N a node n such that
    n.S = "S"
    or that it was mapped to N the edge
    n'.De such that n'.D = n and n.S = "P"

    In the map n -> M <- {N,N.De} if n is not leaf,
    for each son n' of n there is the map n' -> M' <- {N',N'.De}.
    Since n' < n and assuming t is congruent with T, we can see that N' <= N.
    If N' < N, each gene mapped to N must be inherited from N to some
    nodes N', and it must be lost in other nodes. To determine where these
    events occur it must be reviewed the node n and its sons,
    as described below.

    Given the map n -> N, a gen is mapped to N by a
    node n such taht n.S = "S". Each son n' of n has the map
    n' -> M' <-{N',N.De'}
    K is a list containing one item for each n':
    - if n' is leaf then item = N'
    - if n'.S = "S" then item = N'
    - if n'.S = "P" then item = N'.D
    The gen will be inherited to all the nodes N'' such that
    N''' < N'' < N for some N''' <- K
    The gen will be lost in all the nodes N'' such that
    N''< N and N'' <> N''' for all N''' in K

    Given the map n -> N.De a gen is mapped to N for
    each edge n'.De such that n'.D=n. Here we can take
    the map n' -> M' <-{N',N'.De}.
    The gene will be inherited to all the nodes N'' such that
    N' < N'' < N
    the gen will be lost in all the nodes N'' such that
    N'' < N and N'' <> N'

    Detailed description of atributtes
    ----------------------------------

    As result of reconciliation some attributes are creatted for genes tree:

    - t.map
      [Dict] Contains the information revelaed by 
      the map of each node of T:

      {
          n : {
                                                            #############################################################
                                                            #                                                           #
                  "N" : N, <================================#  n --> N is the map itself                                #
                                                            #                                                           #
                                                            #############################################################

                                                            #############################################################
                                                            #                                                           #
                  "H" : [[N, N',N'',...]], <================# A list for each gene mapped to N                          #
                                                            # Each list contains the nodes where gene where mapped      #
                                                            #                                                           #
                                                            #############################################################

                                                            #############################################################
                                                            #                                                           #
                  "S" : [ list of { N : Sy }] <=============# A dict for each gene mapped to N                          #
                                                            # keys are the nodes in the corresponding list of H         #
                                                            # and content is an event symbol: spetiation or loss        #
                                                            #                                                           #
                                                            #############################################################
                                                            
                                                            #############################################################
                                                            #                                                           #
                  "n" : [list of { N : int }] <=============# A dict for each gene mapped to N                          #
                                                            # saves the order in which the hineritance is mapped.       #
                                                            # this attribute is important for plot pipe reconciliation  #
                                                            #                                                           #
                                                            #############################################################

              }
      }

    - t.sings
      List

    - t.mapped_num
      Dict that contains the number of objects mapped to
      node and edges of T. The main use of this dictionary is
      to keep the order for the map of a node n in the atribute
      t.map[n]["n"]
      {
          N : {
                  "S" : int ,   # Number of elements mapped to N
                  "P": int      # Number of elements mapped to N-N.dad edge
              }
      }

    

    Attributes of the species tree changes:

    - T.root_duplications

    Attributes of species tree nodes changes:

    - N.weight
    - N.edgesGain
    - N.edgesLose
    - N.singletones

    """
    def __init__(self,t,T,errorWhenNotCongruent=True):
        # Reconciliates genes Tree t with species tree T
        self.t=t
        self.t.sings= []
        self.T=T
        self.T.root_duplications=0
        self.t.mapped_num={}
        self.t.map={}
        self.arecongruent,self.incongruets,self.congruents=areCongruent(t,T)
        self.edades={}
        if(self.arecongruent):
            self.mapRoot()
            bfs=order.Tour(self.t.root)
            bfs.restriction=lambda node: node.isLeaf()
            bfs.actions=[self.mapEvent]
            bfs.condition=lambda n : not (n==self.t.root and n.type=="P")
            bfs.go()
        elif(errorWhenNotCongruent):
            raise ValueError("Species tree\n{}\n\ndoesn't displays genes tree\n{}".format(T.root.labelAsSuperNode(),t.root.labelAsSuperNode()))

    def mapRoot(self):
        # maps the genes loses for species of T not apearing in the branch of T induced by
        #    the las common ancestor of the species involved in t.
        targets=self.t.root.inducedLeavesType
        nodeRestriction=self.T.typesLastCommonAncestor(targets)
        restriction=lambda node: node==nodeRestriction
        if(self.t.root.type=="S"):
            #self.inherit(targets,restriction,root=self.T.root,node_mapped=None)
            #nodeRestriction.sumGene()
            nodeRestriction.weight-= 1
            nodeRestriction.singletones+= 1
            self.t.genesAge= nodeRestriction.age
        else:
            self.t.genesAge= nodeRestriction.age-1
            if(nodeRestriction==self.T.root):
                self.T.root_duplications+= len(self.t.root.soons)-1
            else:
                #self.inherit(targets, restriction=restriction , root= self.T.root, node_mapped=self.t.root)
                nodeRestriction.dad.edgesGain_singletones[nodeRestriction]+= len(self.t.root.soons)
                #nodeRestriction.duplicateGene(len(self.t.root.soons)-1)
            for son in self.t.root.soons:
                restrictor1= self.getDuplicationRestrictor(son)
                restriction= lambda node: node==restrictor1
                self.inherit(son.inducedLeavesType, restriction=restriction , root= nodeRestriction, node_mapped=self.t.root)

    def mapEvent(self,node):
        if(node.type=="S"): self.mapSpetiation(node)
        elif(node.type=="P"):
            self.mapDuplication(node)

    def mapSpetiation(self,node):
        NodeRoot=self.T.typesLastCommonAncestor(node.inducedLeavesType)
        restrictors=self.getSpetiationRestrictors(node)
        self.inherit(node.inducedLeavesType,restriction=lambda node: node in restrictors,node_mapped=node)
        self.t.map[node]["S"][None][NodeRoot]="ro"

    def mapDuplication(self,node):
        dupNodeRoot=self.T.typesLastCommonAncestor(node.inducedLeavesType)
        dupNodeRoot.duplicateGene(len(node.soons)-1)

        for soon in node.soons:
            restrictor=self.getDuplicationRestrictor(soon)
            self.inherit(
                soon.inducedLeavesType,
                restriction=lambda node: node==restrictor,
                root=dupNodeRoot,
                node_mapped=node,
                node_mapped_son=soon)

        self.t.mapped_num[dupNodeRoot] = self.t.mapped_num.get(dupNodeRoot, {"S":0, "P": 0} )
        self.t.mapped_num[dupNodeRoot]["P"]+=1
        self.t.map[node]["H"][None]=[]
        self.t.map[node]["n"][None]= { dupNodeRoot : {
            "S" : None,
            "P" : self.t.mapped_num[dupNodeRoot]["P"]}}
        self.t.map[node]["S"][None]= { dupNodeRoot : "bD"}

    def upDateCount(self,node,M,node_mapped_son,node_S_or_P, pos_P= False):
        """
        - node_S_or_P 
          List containing on or both of ["S" , "P" ]
          The lements of the list indicates where were mapped a gen
          in a node or in it dad's edge or both.

        - pos_P
          
        """
        M["H"][node_mapped_son].append(node)
        M["n"][node_mapped_son][node]= {}
        self.t.mapped_num[node]= self.t.mapped_num.get(node, {"S":0, "P": 0})
        # 
        if("P" in node_S_or_P):
            self.t.mapped_num[node]["P"]+= 1
            M["n"][node_mapped_son][node]["P"]= self.t.mapped_num[node]["P"]
        elif(pos_P):
            M["n"][node_mapped_son][node]["P"]= self.t.mapped_num[node]["P"]
        else:
            M["n"][node_mapped_son][node]["P"]= None
        if("S" in node_S_or_P):
            self.t.mapped_num[node]["S"]+= 1
            M["n"][node_mapped_son][node]["S"]= self.t.mapped_num[node]["S"]
        else:
            M["n"][node_mapped_son][node]["S"]= None

    def get_updater_h(self, node, node_mapped, M, node_mapped_son):
        f0= M["N"]==node
        f= node_mapped.type=="P"
        ff= node_mapped.tree.root==node_mapped
        fff= node_mapped.dad!= None and node_mapped.dad.type=="P"
        #update0= lambda node, types: self.upDateCount(node, M, node_mapped_son, types)
        if( f0 and (f or ff)):
            self.upDateCount(node, M, node_mapped_son, ["S"])
        elif(fff and node_mapped.tree.map[node_mapped.dad]["N"]==node):
            self.upDateCount(node, M, node_mapped_son, ["S"], pos_P= True)
        else:
            self.upDateCount(node, M, node_mapped_son, ["S", "P"])

        #update1= lambda node : update0(node, ["S"]) if ((f or ff) and M["N"]==node) else update0(node, ["S", "P"])
        #update1(node)

    def inherit(self,targetTypes,restriction,root=None,node_mapped=None, node_mapped_son=None):
        self.ttargetTypes=targetTypes
        if(root==None):
            root=self.T.typesLastCommonAncestor(targetTypes)
        if(node_mapped!=None):
            if(node_mapped in self.t.map):
                M=self.t.map[node_mapped]
                M["H"][node_mapped_son]= [] #[[]]
                M["S"][node_mapped_son]= {} #[{}]
                M["n"][node_mapped_son]= {} #[{}]
            else:
                M={"N" : root, "H" : {node_mapped_son: []}, "S" : {node_mapped_son: {}}, "n" : {node_mapped_son: {}}}
                self.t.map[node_mapped]=M

            """
            f= node_mapped.type=="P"
            ff= node_mapped.tree.root==node_mapped
            fff= node_mapped.dad!= None and node_mapped.dad.type=="P"

            update0= lambda node, types: self.upDateCount(node, M, node_mapped_son, types)
            update1= lambda node : update0(node, ["S"]) if ((f or ff) and M["N"]==node) else update0(node, ["S", "P"])
            """

            update1= lambda node : self.get_updater_h(node, node_mapped, M, node_mapped_son)

            acts=[lambda node: M["S"][node_mapped_son].update({node:".r"}),
                  update1]
                  #lambda node: self.upDateCount(node, M, node_mapped_son,["S"])]
            eacts=[lambda node: M["S"][node_mapped_son].update({node:"Xk"}),
                   lambda node: self.upDateCount(node, M, node_mapped_son,["P"])]
        else:
            acts=[]
            eacts=[]
        bfs=order.Tour(root)
        bfs.actions=[lambda node: node.sumGene()] + acts
        bfs.condition=lambda node: len(set(targetTypes).intersection(node.inducedLeavesType))>0
        bfs.elseActions=[lambda node: node.loseGene(), lambda node: bfs.quitSoonFromItems(node)] + eacts
        bfs.quitSoonFromItemsCondition=restriction
        bfs.go()

    def getSpetiationRestrictors(self,node):
        restrictors=[]
        for soon in node.soons:
            if(not soon.isLeaf()): restrictors+=[self.T.typesLastCommonAncestor(soon.inducedLeavesType)]
        return restrictors

    def getDuplicationRestrictor(self,node):
        if(node.isLeaf()): return None
        else: return self.T.typesLastCommonAncestor(node.inducedLeavesType)

def identity(x):
    return x

def x2True(x):
    return True

def editedIncongruentTree(t,T,editions=None):
    """
    Returns a genes tree t' that is congruent with the species tree T
    and is obtained via minCutSuperTree(t,T,genesTree=t), where
    the weight of T is 100 times the weight of t.

    This edition is equivalent to remove the in-set relation in the conflictive triplets
    """
    T_ind=getInducedSubTree_byLeafsType(T,t.root.inducedLeavesType)
    Wf=lambda tree: 100 if tree==T_ind else 1
    new_t,editions=minCutSuperTree([t,T_ind],genesTree=t,W=Wf,editions=editions)
    return new_t

def checkLeafsMap(t,T,mapIdentifier=""):
    types={leaveType:len(t.type2leaves[leaveType]) for leaveType in t.type2leaves}
    for leaveType in types:
        if(types[leaveType]!=T.type2leaves[leaveType][0].weight):
            msg="The number of genes in each leaf of the species tree does not match the bumber of genes of that type in genes tree leafs"
            raise errors.reconciliationError(msg)

class ForestReconciliation:
    """
    Initialization:
    - list of gene trees
    - species tree

    atributes:
    - Original geneTrees
    - reconciliation tree for each input tree or is edition to make it conguent
    - edited gene trees
    - gene trees editions

    methods:
    - Sum reconciliation trees
    - print reconciliation tree
    - Print edge information
    """

    def __init__(self,geneTreesIterable,speciesTree,geneTreesID=None,\
                leaf2geneFunction=lambda node:node.label,leaf2specieFunction=lambda node: node.type,
                display_warinigs=True):
        self.speciesTree=speciesTree
        self.speciesTree.root_duplications=0
        if(geneTreesID==None):
            self.geneTrees={label : geneTree for label,geneTree in enumerate(geneTreesIterable)}
        else:
            self.geneTrees={label : geneTree for label,geneTree in zip(geneTreesID,geneTreesIterable)}
        self.species=[leaf2specieFunction(leaf) for leaf in speciesTree.leavs]
        self.reconciliationTrees=dict()
        self.editedTrees=dict()
        self.errorInEdition=dict()
        self.treeEditions=dict()
        self.inconsistentMaps=[]
        self.genesAntiquity=dict()
        self.gen2tree=dict()
        self.gen2specie=dict()
        self.specie2genes={specie1:[] for specie1 in self.species}
        self.genOrthologous=dict()
        self.reconciliationNodesRelation=dict()
        for ID,t in tqdm(self.geneTrees.items()):
            t.cc=ID
            tc=treeCopier(self.speciesTree)
            T=tc.ctree
            self.reconciliationNodesRelation[ID]=tc.tree2ctree
            T.cc=ID
            T.getTripletBag()
            self.reconciliationTrees[ID]=T
            arecongruent,_,_=areCongruent(t,T)
            if(not arecongruent):
                self.treeEditions[ID]={"S":list(),"P":list()}
                self.editedTrees[ID]=t
                try:
                    t=editedIncongruentTree(t,T,editions=self.treeEditions[ID])
                except:
                    self.errorInEdition[ID]=t
                else:
                    t.cc=ID
                    t.getTripletBag()
            rec=Recon(t,T,errorWhenNotCongruent=False)
            try:
                checkLeafsMap(t,T)
                isSumCongruent,_=wasCorrectMap(T,returnIncongruents=True)
                if(isSumCongruent):
                    self.savegenesAntiquity(t,ID,leaf2geneFunction,leaf2specieFunction)
                elif(display_warinigs):
                    warnings.warn("The tree '{}' have an inconcistent reconciliaton tree. If you use it in trees operation, the result will be wrong. For more inconcistent maps check forestReconciliation.inconsistentMaps list.".format(t.cc))
                    self.inconsistentMaps.append(t.cc)
            except errors.reconciliationError:
                if(display_warinigs):
                    warnings.warn("The tree '{}' have an inconcistent reconciliaton tree. If you use it in trees operation, the result will be wrong. For more inconcistent maps check forestReconciliation.inconsistentMaps list.".format(t.cc))
                self.inconsistentMaps.append(t.cc)
            except: raise
        self.sumIndividualTrees()

    def savegenesAntiquity(self,t,ID,leaf2geneFunction=lambda node:node.label,leaf2specieFunction=lambda node: node.type):
        for leaf in t.leavs:
            gen=leaf2geneFunction(leaf)
            specie=leaf2specieFunction(leaf)
            self.genesAntiquity[gen]=t.genesAge
            self.gen2tree[gen]=t.cc
            self.gen2specie[gen]=specie
            self.specie2genes[specie]+=[gen]
            self.genOrthologous[gen]={specie1:[] for specie1 in self.species}
            for ortholog in getLeavOrthologs(leaf):
                gen1=leaf2geneFunction(ortholog)
                specie1=leaf2specieFunction(ortholog)
                self.genOrthologous[gen][specie1]+=[gen1]

    def sumIndividualTrees(self):
        tour=order.Tour(self.speciesTree.root,actions=[self.sumIndividualNodes])
        tour.go()
        self.speciesTree.root_duplications=sum([
                                                T.root_duplications
                                                for ID,T in self.reconciliationTrees.items()
                                                if ID not in self.inconsistentMaps and ID not in self.editedTrees ])
        self.isSumCongruent,self.SumIncongruents=wasCorrectMap(self.speciesTree,returnIncongruents=True)
        if(not self.isSumCongruent):
            warnings.warn("The sum of the individual reconciliation trees to de species tree is inconsistent. Check if you included any tree of the forestReconciliation.inconsistentMaps list.")

    def sumIndividualNodes(self,node):
        """
        Sums the atributes of the copys of node to the atributes of node
        the atributes are: weight, edgesGain and edgesLose
        """
        node.weight=0
        node.singletones=0
        for son in node.soons:
            node.edgesGain[son]=0
            node.edgesGain_singletones[son]=0
            node.edgesLose[son]=0
        for ID,T in self.reconciliationTrees.items():
            if(ID not in self.inconsistentMaps and ID not in self.editedTrees):
                nodeCopy=self.reconciliationNodesRelation[ID][node]
                node.weight+= nodeCopy.weight
                node.singletones+= nodeCopy.singletones
                for son in node.edgesGain:
                    sonCopy=self.reconciliationNodesRelation[ID][son]
                    node.edgesGain[son]+=nodeCopy.edgesGain[sonCopy]
                    node.edgesGain_singletones[son]+= nodeCopy.edgesGain_singletones[sonCopy]
                    node.edgesLose[son]+=nodeCopy.edgesLose[sonCopy]

    def printNoOrthologous(self,spice):
        with open(self.path+"Orthologs/"+self.projectName+"."+spice+"_notOrthologous"+"."+self.identifierKind+".edgesAge","w") as F:
            F.write("genes_family,age\t"+spice+"\t")
            for spice1 in self.spisIter[:-1]: F.write(spice1+"\t")
            F.write(self.spisIter[-1]+"\n")
            if(self.filterType=="genesList"):
                primero=lambda item: F.write("-,-\t{}\t".format(item))
            elif(self.filterType=="treesList"):
                primero=lambda item: F.write("{},-\t-\t".format(item))
            for item in self.listFilter:
                if(item not in self.filteredList):
                    primero(item)
                    for spice1 in self.spisIter[:-1]:
                        F.write("-\t")
                    F.write("-\n")

    def getGenOrthologous(self,gen,G):
        self.orth={spice1:[] for spice1 in self.spisIter}
        for gen1 in G.edge[gen]:
            spice1=self.genesdicts.gen2spice[gen1]
            if(gen1 not in self.orth[spice1]): self.orth[spice1]+=[gen1]


    def getProteinOrthologous(self,protein,G):
        self.orth={spice1:[] for spice1 in self.spisIter}
        for protein1 in G.edge[protein]:
            spice1=self.genesdicts.protein2spice[protein1]
            if(protein1 not in self.orth[spice1]): self.orth[spice1]+=[protein1]

    def printGenAge(self,gen,age,cc,spice1):
        if(gen in self.printeds): raise errors.existingObject("ID '{}' repeated in the leafs of '{}'".format(gen,cc))
        else: self.printeds+=[gen]
        # escribir a archivos a pares
        for spisF in self.files:
            for gen1 in self.genOrthologous[gen][spisF]:
                self.files[spisF].write("{},{}\t{}\t{}\n".format(cc,age,gen,gen1))
        # imprimir archivo general
        self.F.write("{},{}\t{}\t".format(cc,age,gen))
        for spice1 in self.spisIter[:-1]:
            if(len(self.genOrthologous[gen][spice1])>0):
                for gen1 in self.genOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
                self.F.write("{}\t".format(self.genOrthologous[gen][spice1][-1]))
            else: self.F.write("-\t")
        spice1=self.spisIter[-1]
        if(len(self.genOrthologous[gen][spice1])>0):
            for gen1 in self.genOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
            self.F.write("{}\n".format(self.genOrthologous[gen][spice1][-1]))
        else: self.F.write("-\n")

    def get_congruent_IDs(self):
        return [ID for ID in self.geneTrees if ID not in self.inconsistentMaps]

class plotTree:

    nodeType2symbol={"S":"ro","P":"bD","*":"gx"}

    def __init__(self,tree,figsize=(10,10),lx=1,ly=1,name="flujo.png",saveAndClose=False,printLeafTypeInsteadOfLabel=False,\
                leafLabelDesfase=-0.1,leafWeightDesfase=-0.4,innerNodeWeightDesfase=-0.1,\
                gainEdgeDesface=+0.1,loseEdgeDesface=-0.3,
                fig=None,
                axis_rect=[0,0,1,1],
                linewidth= None, markersize= None, fontsize= None):
        # Inicializar características del dibujo
        if(fig==None):
            self.fig=plt.figure(figsize=figsize)
            self.ax=self.fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
        else:
            self.fig=fig
            self.ax=self.fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
        if(tree.weighted):
            self.printIfWeightNode=self.printNodeWeight
            self.printIfWeightEdge=self.printEdgeWeight
            self.printIfWeightLeaf=self.printLeafWeight
        else:
            self.printIfWeightNode=lambda node:None
            self.printIfWeightEdge=lambda node,son:None
            self.printIfWeightLeaf=lambda node:None
        self.tree=tree
        self.getDiameter()
        self.lx=lx
        self.ly=ly
        self.dx=lx/(self.diameter)
        self.dy=self.ly/(len(tree.leavs)+1)
        self.xNodes={}
        self.yNodes={}
        self.y=self.dy
        self.leafLabelDesfase=leafLabelDesfase
        self.leafWeightDesfase=leafWeightDesfase
        self.innerNodeWeightDesfase=innerNodeWeightDesfase
        self.gainEdgeDesface=gainEdgeDesface
        self.loseEdgeDesface=loseEdgeDesface
        self.linewidth= linewidth
        self.markersize= markersize
        self.fontsize= fontsize
        if(printLeafTypeInsteadOfLabel): self.leafLabel=lambda node: node.type
        else: self.leafLabel=lambda node: node.label
        # Graficar raíz
        #plt.axes(frame_on=False)
        #plt.xticks([])
        #plt.yticks([])
        plt.plot([-self.dx,0],[self.ly/2,self.ly/2],color="black")
        if(self.tree.type=="species" and self.tree.weighted):
            plt.text(
                    -self.dx/2,
                    self.ly/2 +self.innerNodeWeightDesfase*self.dy,
                    "0 ({})".format(self.tree.root_duplications),
                    color="green",
                    fontsize=self.fontsize)
        # Inicializar pos order
        self.permutation=list()
        posOrder=order.Tour(tree.root,
                            kind="DFS",
                            DFSOrder="pos",
                            actions=[self.plotNodeAndSoons],
                            condition=lambda node:not node.isLeaf())
        posOrder.go()
        # Inicializar bottomUp
        if(saveAndClose): plt.savefig(name)

    def printNodeWeight(self,node):
        self.ax.text(
            self.xNodes[node]+0.1*self.dx,
            self.yNodes[node]+self.innerNodeWeightDesfase*self.dy,
            "{} ({})".format(node.weight,node.singletones),color="blue",
            fontsize=self.fontsize)

    def printLeafWeight(self,node):
        self.ax.text(self.xNodes[node]+0.1*self.dx,self.yNodes[node]+self.leafWeightDesfase*self.dy,"{}".format(node.weight),color="blue", fontsize=self.fontsize)

    def printEdgeWeight(self,node,soon):
        self.ax.text(
                    self.xNodes[node]+0.1*self.dx,
                    self.yNodes[soon]+self.gainEdgeDesface*self.dy,
                    "+{} ({})".format(node.edgesGain[soon],node.edgesGain_singletones[soon]),color="green",
                    fontsize=self.fontsize)
        self.ax.text(self.xNodes[node]+0.1*self.dx,self.yNodes[soon]+self.loseEdgeDesface*self.dy,"-{}".format(node.edgesLose[soon]),color="red", fontsize=self.fontsize)

    def plotNodeAndSoons(self,node):
        # Calcular pos de hojas
        self.plotNodeSonsLeafs(node)
        # calcular posición del nodo
        self.xNodes[node]=min([self.xNodes[son] for son in node.soons])-self.dx
        indY=[self.yNodes[indLeav] for indLeav in node.inducedLeaves]
        self.yNodes[node]=(min(indY)+max(indY))/2
        # Crear lineas
        self.printIfWeightNode(node)
        for soon in node.soons:
            X=[self.xNodes[node],self.xNodes[node],self.xNodes[soon]]
            Y=[self.yNodes[node]+0.1*self.dx,self.yNodes[soon],self.yNodes[soon]]
            self.ax.plot(self.xNodes[node],self.yNodes[node],self.nodeType2symbol[node.type],zorder=2,markersize=self.markersize)
            self.ax.plot(X,Y,color="black",zorder=1, linewidth= self.linewidth)
            self.printIfWeightEdge(node,soon)

    def plotNodeSonsLeafs(self,node):
        for soon in node.soons:
            if(soon.isLeaf()):
                self.permutation.append(soon)
                self.xNodes[soon]=self.lx
                self.yNodes[soon]=self.y
                self.ax.text(self.lx,self.y+self.leafLabelDesfase*self.dy,self.leafLabel(soon), fontsize=self.fontsize)
                self.printIfWeightLeaf(soon)
                self.y+=self.dy

    def getDiameter(self):
        self.diameter=len(self.tree.leavs[0].path)
        for leave in self.tree.leavs[1:]:
            l=len(leave.path)
            if(l>self.diameter): self.diameter=l

#########################################################################
#                                                                        #
#                MinCutSuperTree                                            #
#                                                                        #
#                Obtiene un árbol con el mínimo númerode                    #
#                tripletas incongruentes con un conjunto                    #
#                de árboles pesados.                                        #
#                                                                        #
#########################################################################

def minCutSuperTree(Trees,W=lambda tree: 1,editions=None,returnTreeType="species",g_i=0,dadTrees=None,
                    leaf2specie=lambda node:node.type,validateTriplets=True):
    """
    Aplies minCutSupertree algorithm [Semple C. and Steel M. (2000) A supertree method for rooted trees.]

    For detailed description of algorithm and outputs see 'Detailed description' at the bottom.

    You can:

    - get a tree with minimun set of incogruent triplets with a list of Trees.

    - Edit a genes tree to get congruent with a species tree (see genes_tree parameter).

    - Avoid triplets in the output tree that are not-resolved triplets in
      a guide_tree (see guide_tree parameter).

    OutPut:

    - Tree with minimun set of incongruent triplets with the Trees input list.

    - Dictionary of editions.
      if genesTree provided, 

    Input:

    - Trees:
      List of evo.Tree objetcs

      The leaf type of the trees is taken in account for the creation
      of the graph St that contains information about in-group relations
      between the leafs of the trees.

    - W  [default: function W : any_tree ----> 1 ]:
      Function whos input is a tree and it's output is a number >0.

      Whe default function is  returns 1 for any tree: W(any_tree)=1

    - genesTree [default: None]:
      evo.Tree object

      if genesTree=None then the return will be an species tree

      else the return will be a modification of the provided genesTree
      obtained by eliminatin in-set relations between leafs in the
      genesTree (as the minCutAlgorithm does when finds incongruent groups).

      If you want to edit a genes tree t to be congruent with an
      species tree T you must set:

      - Trees parameter equals to a list [ t , T ].

      - genesTree = t

      - the weight function must give more evidence
        to the species tree, as example: W(T)=100*w(t)

    - guide_tree:
        evo.Tree ojtect

        - Use this parameter to avoid triplets in the output tree that
          are not-resolved triplets in the given guide_tree.

        - You also must set w_guide_tree greather than cero.

        - The non-resolved triplets of the guide_tree will preserve
          non-resolved in the output only if w_guide_tree > 0

        - The guide tree is also used as a tree en the Trees input list.

        - If you want to preserve all the triplets of guide_tree in the output tree
          then w_guide_tree must be grather than the sum of the weihts of all the
          other trees privided.

    Detailed description:

      The leaf type of the trees is taken in account for the creation
      of the graph St that contains information about in-group relations
      between the leafs of the trees.
    """
    g_i+=1
    outTrees=[T for T in Trees if T.type==returnTreeType]
    if(dadTrees==None):
        dadTrees={tree:tree for tree in Trees} # tree : original_tree
    # Initialice label2spice Dict
    label2spice=dict()
    for T in outTrees:
        label2spice.update(T.label2spice)
    # Get the species for the St graph
    S=set((frozenset([leaf2specie(s)]) for s in chain.from_iterable([T.leavs for T in Trees])))
    # Initialice editions dict
    if(editions==None):
        editions=set()
    # MinCutDupertree
    if(len(S)>2):
        # St keeps the information of in-set relations between the type of the leafs in the diferent Trees
        St=properSubSetGraph(Trees,S,W,leaf2specie)
        # The conected components are non-conflictive groups of nothes, but we need at least 2
        S=basicFunctions.conComp(St)
        if(len(S)==1):
            # If conly one CC in St, then create an St_Et graph and apply it's minCut to St
            St_Et=getSt_Et(St,W,Trees)
            cutGComputedInGs(St,St_Et)
            S=basicFunctions.conComp(St)

            SpeciesPartition=[frozenset(chain.from_iterable(s)) for s in S]
            # Obtain edit set for the outTrees
            # Otain edited trees
            for T in outTrees:
                oldPartition=frozenset(properSubsets(T,leaf2elementOfSubSet=lambda leaf: leaf2specie(leaf)))
                leafsType=set((leaf2specie(leaf) for leaf in T.leavs))
                newPartition=frozenset(( frozenset(s.intersection(leafsType)) for s in SpeciesPartition if not s.isdisjoint(leafsType) ))
                if(oldPartition!=newPartition):
                    editions.add(dadTrees[T])
        # Para cada conjunto de nodos S_i in S aplica minCutSuperTree a los árboles inducidos por los árboles de entrada y S_i
        T_j=applyMinCutSuperTreeToInducedTrees(S,Trees,W,editions,g_i,returnTreeType,dadTrees,leaf2specie,validateTriplets)
        T=joinTrees(T_j,treetype=returnTreeType,nodeType="S",label2spice=label2spice,validateTriplets=validateTriplets)
        colapseInnerNodes(T,makeDiscriminant=False)
        return T,editions
    else:
        # get type2nodes dict for starTrees type spetiation ('P')
        type2labels=dict()
        for T in outTrees:
            for leafType,nodes in T.type2leaves.items():
                type2labels[leafType]=set((node.label for node in nodes)).union(type2labels.get(leafType,set()))
        type2nodes={leafType:[Node(label=label,type=leafType) for label in labels] for leafType,labels in type2labels.items()}
        # Obtein the nodes for starTree
        if(len(S)==1):
            specie=list(type2nodes)[0]
            st=starTree(type2nodes[specie],nodeType="P",treetype=returnTreeType,label2spice=label2spice)
        else:
            S=[
                starTree(
                    type2nodes[leafType],
                    nodeType="P",
                    treetype=returnTreeType,
                    label2spice=label2spice).root
                if len(type2nodes[leafType]) >1
                else Node(label=type2nodes[leafType][0].label,type=type2nodes[leafType][0].type)
                for leafType in type2nodes]
            # Return species starTree
            st=starTree(S,nodeType="S",treetype=returnTreeType,label2spice=label2spice)
        return st,editions

def getEditPartitions(Trees,dictionary,leaf2elementOfSubSet=lambda leaf:leaf.type):
    added_edges=list()
    new_partition=frozenset([frozenset([leaf2elementOfSubSet(leaf) for leaf in T.leavs]) for T in Trees])
    for son in genesTree.root.soons:
        old_part=frozenset([leaf2elementOfSubSet(leaf) for leaf in son.inducedLeaves])
        posible_added_edges=set()
        for part_n in new_partition:
            inter=old_part.intersection(part_n)
            diff=old_part.difference(part_n)
            posible_added_edges.update([frozenset(pair) for pair in product(inter,diff)])
            old_part=diff
            if(len(old_part)==0):
                break
        old_part=frozenset([leaf2elementOfSubSet(leaf) for leaf in son.inducedLeaves])
        bfsActions=[lambda node : compareSubSetPartitionEdges(node,posible_added_edges,old_part,added_edges)]
        bfsCondition=lambda node: len(posible_added_edges)>0 and len(node.soons)>1 and not node.type==branchType
        bfs=order.Tour(son,kind="BFS",actions=bfsActions,condition=bfsCondition)
        bfs.go()
    dictionary[branchType]+=[tuple(edge) for edge in added_edges]

def compareSubSetPartitionEdges(node,posible_added_edges,old_part,added_edges):
    node_partition=set(properSubsets(None,node=node,leaf2elementOfSubSet=lambda leaf:leaf.label))
    node_edges=set([frozenset((a,b)) for A,B in combinations(node_partition,2) for a,b in product(A,B)])
    inter=posible_added_edges.intersection(node_edges)
    added_edges.extend(inter)
    posible_added_edges.difference_update(inter)


def partitonFromGenesTreesLeafsLabels(T_genes):
    partition=[]
    for T in T_genes:
        labels=[leaf.label for leaf in T.root.inducedLeaves]
        partition.append(labels)
    return partition

def getSt_Et(St,W,Trees):
    # Crear St_Et
    St_Et=St.copy()
    w_max=sum((W(tree) for tree in Trees))
    compact_w_edges(St_Et,Trees,W,w_max)
    return St_Et

def cutGComputedInGs(St,St_Et):
    Wcut,partitions=stoerwagner.stoer_wagner(St_Et,returnAll=True)
    cutSet=[]
    for A,B in partitions:
        A=[frozenset([a]) for a in chain.from_iterable(A)]
        B=[frozenset([b]) for b in chain.from_iterable(B)]
        ecut=basicFunctions.getEdgesBetween(A,B,St,weighted=False)
        St.remove_edges_from(ecut)
        cutSet+=ecut
    return cutSet

def compact_w_edges(St,Trees,W_function,W_max):
    """
    Comprime todas las aristas con peso w.
    Se borran las aristas paralelas y se remplazan
    con una arista cullo peso es la suma de los pesos
    de los árboles que tienen un subconjunto propio que
    contiene los extremos de almenos una arista en esa clase
    paralela
    """
    # Por cada arista en Et Contraer arsita, Quitar y remplazar aristas paralelas
    mergedNodes={node:node for node in St.nodes()}
    getNode=lambda node: mergedNodes[node]
    for aa,bb in get_w_edges(St,W_max):
        a,b=map(getNode,(aa,bb))
        if(a!=b):
            # Crear nodo comprimido
            Nc=frozenset(a.union(b))
            for baseNode in Nc:
                mergedNodes[frozenset([baseNode])]=Nc
            St.add_node(Nc)
            # Poner peso de las aristas paralelas = número de árboles en los que leavs es un subconjunto propio del árbol con las hojas en los extremos de las aristas 
            # Los vecions co los que tendrán aristas paralelas son aquellos que están en la interección de las vecindades de los nodos en los extremos de la arista a contraer
            for vecino in set(St.neighbors(a)).intersection(St.neighbors(b)):
                newPss=Nc.union(vecino)
                for T in Trees:
                    if(isProperSubset(T,newPss)):
                        if(St.has_edge(vecino,Nc)): St[vecino][Nc]['weight']+=W_function(T)
                        else: St.add_edge(vecino,Nc,weight=W_function(T))
            St.remove_nodes_from((a,b))

def getInducedSubTree_byLeafsType(T,leafsTypeSet,getDiscriminantTree=False,restriction=None,Wdict=dict(),Wfunction=lambda tree:None):
    leafsTypeSet=set(leafsTypeSet)
    inter=leafsTypeSet.intersection(T.type2leaves)
    leafsSet=set(chain.from_iterable([T.type2leaves[leafType] for leafType in inter]))
    if(restriction==None):
        restriction=lambda node:len(inter.intersection(node.inducedLeavesType))==0
    return getInducedSubTree(T,leafsSet,getDiscriminantTree=getDiscriminantTree,
                            restriction=restriction,
                            Wdict=Wdict,Wfunction=Wfunction)

def getInducedSubTree(T,leafsSet,getDiscriminantTree=False,restriction=None,Wdict=dict(),Wfunction=lambda tree:None):
    """
    Returns the induced sub tree of T such that
    all leafs of the induced tree are contained in the leafsSet.
    If the tree has no leafs of type in the leafSet then returns None
    """
    leafsSet=set(leafsSet)
    inter=leafsSet.intersection(T.leavs)
    if(len(inter)>0):
        if(restriction==None):
            restriction=lambda node: len(inter.intersection(node.inducedLeaves))==0
        Tcop=treeCopier(T,root=T.nodesLastCommonAncestor(inter),restriction=restriction).ctree
        Wdict[Tcop]=Wfunction(T)
        return Tcop
    else:
        return None

def colapseInnerNodes(T,makeDiscriminant=False):
    bfsActions=[T.collapseNode]
    if(makeDiscriminant): isNotDiscriminant=lambda node: False if node.dad==None else node.type==node.dad.type 
    else: isNotDiscriminant=lambda node: False
    bfsCondition=lambda node: len(node.soons)==1 or isNotDiscriminant(node)
    bfs=order.Tour(T.root,kind="BFS",actions=bfsActions,condition=bfsCondition)
    bfs.go()


def getInducedTrees(Trees,S_i,W,dadTrees,leaf2specie=lambda leaf: leaf.type):
    indTrees=list()
    WW=dict()
    for T in Trees:
        leafs=[leaf for leaf in T.leavs if leaf2specie(leaf) in S_i]
        Tcop=getInducedSubTree(T,leafs,Wdict=WW,Wfunction=W)
        if(Tcop!=None):
            dadTrees[Tcop]=dadTrees[T]
            indTrees.append(Tcop)
    return indTrees,WW

def properSubSetGraph(Trees,nodesSet,w_function,leaf2specie):# nodeFunction=lambda node:frozenset([node.type])):
    """
    Construir grafo donde la arista (A,B,c) significa que A y B están en el mismo cluster propio de c árboles en Trees
    Inputs:
    - Trees: List of trees evo.Tree type species
    - S: Set of
    - nodeFunction: function that converts leaf of tree to node of graph
    Output:
    - weigted networkx graph
    """
    edges=dict()
    for T in Trees:
        for pss in properSubsets(T,leaf2elementOfSubSet=lambda node:frozenset([leaf2specie(node)])):
            for a,b in combinations(pss,2):
                edge=frozenset((a,b))
                edges[edge]=edges.get(edge,0)+w_function(T)
    St=nx.Graph()
    St.add_nodes_from(nodesSet)
    St.add_weighted_edges_from([(a,b,w) for (a,b),w in edges.items()])
    return St

def properSubsets(T,node=None,leaf2elementOfSubSet=lambda leaf:leaf):
    """
    Returns the list of the greaters proper sub sets of A.
    """
    if(node==None): node=T.root
    for son in node.soons:
        yield frozenset([leaf2elementOfSubSet(leaf) for leaf in son.inducedLeaves])

def isProperSubset(T,comparationSet,leavs2comparation=lambda leav:leav.type):
    """
    Return True if the set leavs is a proper sub set of T
    """
    ret=False
    for pss in properSubsets(T,leaf2elementOfSubSet=leavs2comparation):
        if(comparationSet.issubset(pss)):
            ret=True
            break
    return ret

def get_w_edges(St,w):
    """
    Retorna aristas del grafo St con peso w
    """
    return [frozenset((E[0],E[1])) for E in St.edges(data=True) if E[2]['weight']==w]

def nodeLeafSetIsDisjoint(node,leafSet,node2leafs=lambda node:node.inducedLeavesType):
    return len(set(leafSet).intersection(node2leafs(node)))==0

def getLeaveTypeInducedTree(tree,leavTipes):
    S=set(leavTipes)
    # Si S no es un subconjunto de las hojas self, entonces no se puede inducir el árbol.
    if(S<=set(tree.type2leaves)):
        restriction=lambda node: len(S.intersection(node.inducedLeavesType))==0
        root=tree.typesLastCommonAncestor(leavTipes)
        return treeCopier(tree,restriction=restriction,root=root).ctree
    else: return None

def joinTrees(T_j,treetype=None,nodeType="S",weight=False,label2spice=None,validateTriplets=True):
    """
    Returns a new tree where the root's sons are the roots of the list of trees T_j
    """
    if(treetype==None): treetype=T_j[0].type
    if(label2spice==None): label2spice=T_j[0].label2spice
    rootName='('+",".join([Tj.root.labelAsSuperNode(type=True) for Tj in T_j])+')'+nodeType
    root=Node(label=rootName,type=nodeType,path=[])
    for t_j in T_j:
        root.addSoon(node=t_j.root)
    return Tree(root=root,type=treetype,label2spice=label2spice,validateTriplets=validateTriplets)

def starTree(S,nodeType="S",treetype="species",label2spice=None):
    """
    Returns tree where the root's children are a copy of the nodes in the list S.
    """
    rootName='('+",".join([s.labelAsSuperNode(type=True) for s in S])+')'+nodeType
    root=Node(label=rootName,type=nodeType,path=[])
    for s in S:
        root.addSoon(node=s)
    return Tree(root=root,type=treetype,label2spice=label2spice)

def applyMinCutSuperTreeToInducedTrees(S,Trees,W,editions,g_i,returnTreeType,dadTrees,leaf2specie,validateTriplets):
    T_j=list()
    for S_i in S:
        S_i=set(chain.from_iterable(S_i))
        indTrees,WW=getInducedTrees(Trees,S_i,W,dadTrees,leaf2specie=leaf2specie)
        WWW=lambda tree:WW[tree]
        trees,_=minCutSuperTree(indTrees,W=WWW,editions=editions,g_i=g_i,
                    returnTreeType=returnTreeType,dadTrees=dadTrees,leaf2specie=leaf2specie,validateTriplets=validateTriplets)
        T_j.append(trees)
    return T_j