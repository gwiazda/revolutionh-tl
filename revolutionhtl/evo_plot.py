import matplotlib.pyplot as plt
from .upsetplot import from_contents
from .upsetplot import generate_counts
from .upsetplot import plot
from .upsetplot import from_memberships
from . import order
from . import evolutionaryTree as evo
import pandas as pd

nodeType2symbol={"S":"ro","P":"bD","*":"gx"}

############################################
#                                          #
#    Plot Reconciliation tree and upSet    #
#                                          #
############################################

def plot_recon_upSet(forestReconciliation,
                     title="upSet reconciliation",
                     leafs_filter=None,
                     fig=None,
                     figsize=(20,10),
                     rect_left=(-1,0.125,1,0.45),
                     intersection_axis_label= None ):
    T= forestReconciliation.speciesTree
    forestReconciliation.speciesTree.weighted=True
    if(leafs_filter!= None):
        T= evo.getInducedSubTree(T,filter( lambda leaf: leaf.label in leafs_filter, T.leavs))
    species= list(T.type2leaves)
    valid_memberships=set()
    order.Tour(T.root,actions=[lambda node: valid_memberships.add(frozenset(node.inducedLeavesType))]).go()
    ocurrences=dict()

    avoid= forestReconciliation.inconsistentMaps + list(forestReconciliation.editedTrees)

    for ID,t in forestReconciliation.geneTrees.items():
        if(ID not in avoid):
            member=frozenset(t.root.inducedLeavesType)
            ocurrences[member]=ocurrences.get(member,0)+1
            
            """
            if("MOUSE" in t.root.inducedLeavesType):
                for leaf in t.type2leaves["MOUSE"]:
                    orths= forestReconciliation.genOrthologous[leaf.label]
                    orths=frozenset(list(filter(lambda a: a!="MOUSE" and len(orths[a])>0 ,list(orths)))+["MOUSE"])
                    if(orths == member):
                        ocurrences[member]=ocurrences.get(member,0)+1
            """
            
    fig= _plot_tree_and_upSet(T ,ocurrences, fig=fig, figsize=figsize, rect_left=rect_left, intersection_axis_label= intersection_axis_label)
    fig.suptitle(title, fontsize=36)
    return fig

def _plot_tree_and_upSet(T, ocurrences, fig=None, figsize=(20,10), rect_left=(-1,0.125,1,0.45),rect_right= (0,0,1,1), intersection_axis_label=None ):
    #rect_left= left, bottom, width, height
    memberships=list()
    valid_memberships=set()
    order.Tour(T.root,actions=[lambda node: valid_memberships.add(frozenset(node.inducedLeavesType))]).go()
    data=list()
    for member,number in ocurrences.items():
        if(member in valid_memberships):
            memberships.append(member)
            data.append(number)

    if(fig==None):
        fig=plt.figure(figsize=figsize)

    plotTree=PlotTree(T,
                 saveAndClose=False,
                 printLeafTypeInsteadOfLabel=False,
                 figsize= figsize,
                 fig=fig,
                 axis_rect=rect_left,print_leafs_label=False)


    permutation=[leaf.label for leaf in plotTree.permutation]

    df = pd.DataFrame([{name: True for name in names} for names in memberships], columns=permutation)
    #df.sort_index(axis=1, inplace=True)
    df.fillna(False, inplace=True)
    df = df.astype(bool)
    df.set_index(list(df.columns), inplace=True)
    data = pd.Series(data)
    data.index = df.index
    df=data

    ax_upSet= fig.add_axes(rect= rect_right)

    plot( df, fig= fig, sort_categories_by= None , intersection_axis_label=intersection_axis_label )
    return fig

##################################
#                                #
#    Plot Reconciliation tree    #
#                                #
##################################

class PlotTree:

    def __init__(self,tree,figsize=(10,10),lx=1,ly=1,name="flujo.png",saveAndClose=False,printLeafTypeInsteadOfLabel=False,\
                leafLabelDesfase=-0.1,leafWeightDesfase=-0.4,innerNodeWeightDesfase=-0.1,\
                gainEdgeDesface=+0.1,loseEdgeDesface=-0.3,
                fig=None,
                axis_rect=[0,0,1,1],
                linewidth= None, markersize= None, fontsize= None,print_leafs_label=True):
        # Inicializar características del dibujo
        self.print_leafs_label=print_leafs_label
        if(fig==None):
            self.fig=plt.figure(figsize=figsize)
            self.ax=self.fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
        else:
            self.fig=fig
            self.ax=self.fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
        if(tree.weighted):
            self.printIfWeightNode=self.printNodeWeight
            self.printIfWeightEdge=self.printEdgeWeight
            self.printIfWeightLeaf=self.printLeafWeight
        else:
            self.printIfWeightNode=lambda node:None
            self.printIfWeightEdge=lambda node,son:None
            self.printIfWeightLeaf=lambda node:None
        self.tree=tree
        self.getDiameter()
        self.lx=lx
        self.ly=ly
        self.dx=lx/(self.diameter)
        self.dy=self.ly/(len(tree.leavs)+1)
        self.xNodes={}
        self.yNodes={}
        self.y=self.dy
        self.leafLabelDesfase=leafLabelDesfase
        self.leafWeightDesfase=leafWeightDesfase
        self.innerNodeWeightDesfase=innerNodeWeightDesfase
        self.gainEdgeDesface=gainEdgeDesface
        self.loseEdgeDesface=loseEdgeDesface
        self.linewidth= linewidth
        self.markersize= markersize
        self.fontsize= fontsize
        if(printLeafTypeInsteadOfLabel): self.leafLabel=lambda node: node.type
        else: self.leafLabel=lambda node: node.label
        # Graficar raíz
        #plt.axes(frame_on=False)
        #plt.xticks([])
        #plt.yticks([])
        plt.plot([-self.dx,0],[self.ly/2,self.ly/2],color="black")

        # Inicializar pos order
        self.permutation=list()
        posOrder=order.Tour(tree.root,
                            kind="DFS",
                            DFSOrder="pos",
                            actions=[self.plotNodeAndSoons],
                            condition=lambda node:not node.isLeaf())
        posOrder.go()
        # Inicializar bottomUp
        if(saveAndClose): plt.savefig(name)

    def printNodeWeight(self,node):
        text=""
        if( node.weight > 0 ):
            text+= str(node.weight)
        if( node.singletones > 0 ):
            text+= " ({})".format(node.singletones)
        self.ax.text(
            self.xNodes[node]+0.1*self.dx,
            self.yNodes[node]+self.innerNodeWeightDesfase*self.dy,
            text,
            color="blue",
            fontsize=self.fontsize)

    def printLeafWeight(self,node):
        if(node.singletones > 0):
            w= node.weight - node.singletones
            text= f"{w} [{node.singletones}]"
        else:
            text= f"{node.weight}"
        self.ax.text(
            self.xNodes[node]+0.1*self.dx,
            self.yNodes[node]+self.leafWeightDesfase*self.dy*(1+self.print_leafs_label)/2,
            text,
            color="blue",
            fontsize=self.fontsize)

    def printEdgeWeight(self,node,soon):
        w= node.edgesGain[soon] + node.edgesGain_singletones[soon]
        if(w>0):
            text="+"+str(w)
            """
            text="+"
            if( node.edgesGain[soon] > 0 ):
                text+= "{}".format(node.edgesGain[soon])
            if( node.edgesGain_singletones[soon] > 0 ):
                text+= " ({})".format(node.edgesGain_singletones[soon])
            """
            self.ax.text(
                    self.xNodes[node]+0.1*self.dx,
                    self.yNodes[soon]+self.gainEdgeDesface*self.dy,
                    text,
                    color="green", 
                    fontsize=self.fontsize)
        w= node.edgesLose[soon]
        if(w>0):
            self.ax.text(
                        self.xNodes[node]+0.1*self.dx,
                        self.yNodes[soon]+self.loseEdgeDesface*self.dy,
                        "-{}".format(w),
                        color="red",
                        fontsize=self.fontsize)

    def plotNodeAndSoons(self,node):
        # Calcular pos de hojas
        self.plotNodeSonsLeafs(node)
        # calcular posición del nodo
        self.xNodes[node]=min([self.xNodes[son] for son in node.soons])-self.dx
        indY=[self.yNodes[indLeav] for indLeav in node.inducedLeaves]
        self.yNodes[node]=(min(indY)+max(indY))/2
        # Crear lineas
        self.printIfWeightNode(node)
        for soon in node.soons:
            X=[self.xNodes[node],self.xNodes[node],self.xNodes[soon]]
            Y=[self.yNodes[node]+0.1*self.dx,self.yNodes[soon],self.yNodes[soon]]
            self.ax.plot(self.xNodes[node],self.yNodes[node], nodeType2symbol[node.type],zorder=2,markersize=self.markersize)
            self.ax.plot(X,Y,color="black",zorder=1, linewidth= self.linewidth)
            self.printIfWeightEdge(node,soon)

    def plotNodeSonsLeafs(self,node):
        for soon in node.soons:
            if(soon.isLeaf()):
                self.permutation.append(soon)
                self.xNodes[soon]=self.lx
                self.yNodes[soon]=self.y
                if(self.print_leafs_label):
                    self.ax.text(self.lx,self.y+self.leafLabelDesfase*self.dy,self.leafLabel(soon), fontsize=self.fontsize)
                self.printIfWeightLeaf(soon)
                self.y+=self.dy

    def getDiameter(self):
        self.diameter=len(self.tree.leavs[0].path)
        for leave in self.tree.leavs[1:]:
            l=len(leave.path)
            if(l>self.diameter): self.diameter=l

#############################
#                           #
#    Pipe Reconciliation    #
#                           #
#############################

def plot_pipe_recon(t, T, lx=1, ly=1, fig=None, axis_rect=[0,0,1,1], figsize=None, title="trees reconciliation"):
    """
    Before this function you must run: evolutionaryTree.Recon(t,T)

    inputs:
      - t
        evo.Tree such that t.type in ["genes" , "proteins"]
      - T
        evo.Tree such that t.type = "species"
      - axis_rect
        List containing coordinates for figure axis: [left, bottom, width, height]
        Defailt: [0,0,1,1]
    """

    n=len(T.leavs)
    width=lx/(n+1)
    #width=lx*2/(3*n-1)
    dx= width/2
    #dx=width/(n-1)
    dy=ly/(T.diameter+1)

    T.i=0
    order.Tour(T.root, kind="DFS", DFSOrder="pos",actions=[lambda n:get_points(n, dx, dy, width)]).go()
    del T.i


    if(fig==None):
            fig=plt.figure(figsize=figsize)
            ax=fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
    else:
        fig=fig
        ax=fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])

    n=T.leavs[-1].dad
    n=T.root

    #plt.figure(figsize=(20,15))
    plt.fill(n.X+[n.X[0]],n.Y+[n.Y[0]],color="#a1c7ae", zorder=0)
    #, linewidth= 10, markersize= 25
    order.Tour(t.root,actions=[lambda n: get_pos(n,width)],condition=lambda n: not n.isLeaf()).go()
    fig.suptitle(title, fontsize=20)

def plot_pipe_2(forestReconciliation,
                subset= None,
                lx=1,
                ly=1,
                fig=None,
                axis_rect=[0,0,1,1],
                figsize=None):
    """
    Before this function you must run: evolutionaryTree.Recon(t,T)

    inputs:
      - t
        evo.Tree such that t.type in ["genes" , "proteins"]
      - T
        evo.Tree such that t.type = "species"
      - axis_rect
        List containing coordinates for figure axis: [left, bottom, width, height]
        Defailt: [0,0,1,1]
    """
    if(subset==None):
        f= lambda x : True
    else:
        f= lambda x : x in subset
    avoid= forestReconciliation.inconsistentMaps + list(forestReconciliation.editedTrees)
    trees_list= [(ID,t) for ID,t in forestReconciliation.geneTrees.items() if ID not in avoid and f(ID)]

    __fix_tree_position(forestReconciliation,trees_list)
    T= forestReconciliation.speciesTree
    n=len(T.leavs)
    width=lx/(n+1)
    #width=lx*2/(3*n-1)
    dx= width/2
    #dx=width/(n-1)
    dy=ly/(T.diameter+1)

    T.i=0
    order.Tour(T.root, kind="DFS", DFSOrder="pos",actions=[lambda n:get_points(n, dx, dy, width)]).go()
    del T.i


    if(fig==None):
            fig=plt.figure(figsize=figsize)
            ax=fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])
    else:
        fig=fig
        ax=fig.add_axes(axis_rect,frame_on=False,xticks=[],yticks=[])

    n=T.leavs[-1].dad
    n=T.root

    #plt.figure(figsize=(20,15))
    plt.fill(n.X+[n.X[0]],n.Y+[n.Y[0]],color="#a1c7ae", zorder=0)
    #, linewidth= 10, markersize= 25

    """
    def updatePoints(N):
        Np= forestReconciliation.inv_relation[N]
        N.X= Np
    """

    for ID,t in trees_list:
        T= forestReconciliation.reconciliationTrees[ID]
        T.i=0
        order.Tour(T.root, kind="DFS", DFSOrder="pos",actions=[lambda n:get_points(n, dx, dy, width)]).go()
        del T.i

        order.Tour(t.root,actions=[lambda n: get_pos(n,width)],condition=lambda n: not n.isLeaf() ).go()

def __fix_tree_position(forestReconciliation,trees_list):
    
    # forestReconciliation.reconciliationNodesRelation
    #
    # { ID -> { N -> Np } }
    # N is node of the tree with all reconciliation
    # Np is node of a tree with the reconciliation of some genes tree named ID

    # inv_relation
    #
    # { ID -> { Np -> N } }

    inv_relation= {}
    for ID,t in trees_list:
        inv_relation[ID]= {}
        D= inv_relation[ID]
        R= forestReconciliation.reconciliationNodesRelation[ID]
        order.Tour(forestReconciliation.speciesTree.root, actions=[lambda N : D.update({ R[N] : N }) ] ).go()

    #_ Obtener el mapped num total y delta para cada árbol

    mapped_num={} # {N -> {event -> int}} Save the sum of all reconciliations
    mapped_delta={} # {ID -> {N -> {event -> int}}} Save the cumulative sum for each reconciliation

    for ID,t in trees_list:
        mapped_delta[ID]= {}
        R= inv_relation[ID] # { Np -> N }

        for N, D in t.mapped_num.items(): # N is forestReconciliation.reconciliationTrees[ID]
            Np= R[N]
            # Calcula el desplazamiento del nodo actual
            mapped_num[ Np ]= mapped_num.get( Np, {"S" : 0, "P" : 0} )
            mapped_delta[ID][N]= dict(mapped_num[ Np ])
            # Guarda el desplazamiento para el siguiennnte nodei
            mapped_num[ Np ]["S"]+= D["S"]
            mapped_num[ Np ]["P"]+= D["P"]

    #_ Actualizar los parámetros de orden de mapeo del árbol
    for ID,t in trees_list:
        R= inv_relation[ID]
        for N, D in t.mapped_num.items():
            Np= R[N]
            D["S"]= mapped_num[ Np ]["S"]
            D["P"]= mapped_num[ Np ]["P"]
        order.Tour( t.root, actions=[lambda n: __updateMap(n, ID, mapped_delta)] , condition=lambda n: not n.isLeaf() ).go()

def __updateMap(n, ID, mapped_delta):
    t= n.tree
    for N in t.map[n]["H"]: # N is in t , and t.map[n]["H"][N] -> [Nodes of particular tree]
        for Np, ss in t.map[n]["n"][N].items():
            MD= mapped_delta[ID][Np]
            if(ss["S"]==None):
                S_del= None
            else:
                S_del= ss["S"] + MD["S"]
            if( ss["P"] == None ):
                P_del = None
            else:
                P_del= ss["P"] + MD["P"]
            t.map[n]["n"][N][Np]= { "S" : S_del , "P" : P_del }


def get_points(n, dx, dy, width):
    """
    -----------------------------------------------------------------------------------
    | This function must be called for each node in a DFS pos order travel of a tree! |
    -----------------------------------------------------------------------------------

    Asigns to a node n of a tree the shape of the pipe tree induced by n.
    The shape is a serie of dots that can be ploted as plt.plot( n.X, n.Y )

    Propeties
    ---------

    - Each node of the tree is asociated with a segment fo line from (n.X[0] , n.Y[0]) to (n.X[-1] , n.Y[-1])
    - n.Y[0] = n.Y[-1]

    """
    if(n.isLeaf()):
        n.X = [n.tree.i*(dx + width), n.tree.i*dx + (n.tree.i + 1)*width]
        n.Y= [0, 0]
        n.tree.i+=1
    else:
        sons=n.soons[::-1]
        s0, s1= sons[0], sons[-1]
        x0= (s0.X[0] + s1.X[0])/2
        x1= x0 + width
        y=max([s.Y[0] for s in sons])+dy
        n.X=[x0]
        n.Y=[y]
        for s0, s1 in zip(sons[:-1],sons[1:]):
            xa= s0.X[-1]
            ya= s0.Y[-1]
            xb= s1.X[0]
            yb= s1.Y[0]
            dx1a= x1-xa
            dya= y-ya
            dx0b= x0-xb
            dyb= y-yb
            xx= (yb - xb*dyb/dx0b - ya + xa*dya/dx1a)/(dya/dx1a - dyb/dx0b)
            yy= dya*(xx-xa)/dx1a+ya
            n.X+= s0.X + [xx]
            n.Y+= s0.Y + [yy]
        n.X+= s1.X + [x1]
        n.Y+= s1.Y + [y]

def getCenter(X):
    return (X[0] + X[-1])/2

def computeCorrection(Mn,N,n_type,order,width):
    if(N in order):
        delta= width/(Mn[N][n_type]+1)
        step= order[N][n_type]
        return delta*step - width/2
    else:
        #print("N not in order! Check if all is good")
        return 0    

def get_pos(n, width, linewidth= None, markersize= None):
    """
    Plot the map: n --> N
    """
    # Obtain map information
    M= n.tree.map[n]
    N= M["N"]
    Mn= n.tree.mapped_num

    # Obtain inheritance information for n -> N
    symbols= M["S"][None]

    X0, Y0, Xd, Yd, X1, Y1= _determine_map_pos(n, N, Mn, width)

    # Plot n
    if( N.dad!=None and n.dad!=None and N!= n.tree.map[n.dad]["N"] ):
        plt.plot([Xd,X0],[Yd,Y0],"k", linewidth= linewidth, zorder=1)
    if(n.type=="P"):
        plt.plot(X0 ,Y0 , nodeType2symbol["P"], markersize= markersize, zorder=2)
    

    # Plot soon edges
    for ns, L in M["H"].items():
        order= M["n"][ns]
        symbols= M["S"][ns]
        for Ns in L:
            symbol= symbols[Ns]
            if(Ns == N):
                X1p= X1 + computeCorrection(Mn,Ns,"S",order,width)
                plt.plot([X0, X1p],[Y0,Y1],"k", linewidth= linewidth, zorder=1)
                plt.plot(X1p, Y1, symbol, markersize= markersize, zorder=2)
            else:
                X11, Y11, Xd, Yd=map(getCenter, (Ns.X, Ns.Y, Ns.dad.X, Ns.dad.Y))
                X00, Y00= map(getCenter, ((X11,Xd),(Y11,Yd)))
                Xd+= computeCorrection(Mn,Ns.dad,"S",order,width)
                X00+= computeCorrection(Mn, Ns, "P", order, width)
                if(symbol=="Xk"):
                    plt.plot([X00,Xd], [Y00,Yd], "gray", linewidth= linewidth, zorder=1)
                    plt.plot(X00, Y00, symbol, markersize= markersize, zorder=2)
                else:
                    plt.plot([X00,Xd], [Y00,Yd], "k", linewidth= linewidth, zorder=1)
                    X11+= computeCorrection(Mn, Ns, "S", order, width)
                    plt.plot([X00,X11], [Y00,Y11], "k", linewidth= linewidth, zorder=1)
                    plt.plot(X11, Y11, symbol, markersize= markersize, zorder=2)

def _determine_map_pos(n, N, Mn, width):
    # Obtain center of N
    X0= getCenter(N.X)
    Y0= getCenter(N.Y)

    # Determine positions inside node (and edge dad if necesary)
    # - (X0,Y0) is where n were mapped (node if n.type=="S" or edge if n.type=="P")
    # - (Xd,Yd) is where (X0,Y0) come from
    # - (X1,Y1) is where gene(s) asosiated with n were mapped (only relevant if n.type=="P") and come from (X0,Y0)

    X1, Y1= X0, Y0
    D=N.dad

    if(D != None and n.dad!=None):
        #print(n.labelAsSuperNode(type=True))
        Xd=getCenter(D.X)
        Yd=getCenter(D.Y)
        X0= getCenter((X0, Xd))
        Y0= getCenter((Y0, Yd))

        if(n.dad.type=="S"):
            Xd+= computeCorrection(Mn,D,"S", n.tree.map[n.dad]["n"][None],width)
        else:
            Xd+= computeCorrection(Mn, D, "S", n.tree.map[n.dad]["n"][n], width)
        
        X0+= computeCorrection(Mn,N,"P",n.tree.map[n]["n"][None],width)
    else:
        Xd, Yd= None, None
        X0+= computeCorrection(Mn,N,"S",n.tree.map[n]["n"][None],width)
    return X0, Y0, Xd, Yd, X1, Y1


    """
    if(n.type=="P"):
        Xd=getCenter(D.X)
        Yd=getCenter(D.Y)
        X0= getCenter((X0, Xd))
        Xd+= computeCorrection(Mn,D,"S", n.tree.map[n.dad]["n"][None],width)
        Y0= getCenter((Y0, Yd))
    elif(D != None and n.dad!=None):
        Xd=getCenter(D.X)
        Yd=getCenter(D.Y)
        Nd= n.tree.map[n.dad]["N"]
        if( n.dad.type=="P" and Nd==N ):
            Xd= getCenter((X0, Xd))
            Yd= getCenter((Y0, Yd))
            Xd+= computeCorrection(Mn, N, "P", n.tree.map[n.dad]["n"][None], width)
        else:
            Xd+= computeCorrection(Mn, D, "S", n.tree.map[n.dad]["n"][n], width)
    else:
        Xd, Yd= None, None
    X0+= computeCorrection(Mn,N,n.type,n.tree.map[n]["n"][None],width)
    """