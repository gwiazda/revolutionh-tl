# [REvolutionH-tl](https://gitlab.com/jarr.tecn/revolutionh-tl)

Reconstruction of Evolutionary Histories Tool

***

# Documentation

Classes and modules for the inference, reconciliation, and analysis of event-labeled genes tree describing the evolutionary history of genes among different genomes. 

In the pages listed below you can find a detailed description of the tool and the process it performs. You can also start using the package at the [quick start](./tutorial.ipynb), or go to the [main page](../README.md).

**Content**:

1. [General description](#General description)
   1. [Classes](#Classes)
   2. [Modules](#Modules)
2. [Installation](#2. Installation)
   1. [Prerequisites](#2.1 Prerequisites)
   2. [Download and use](2.2 Download and use)

# 1.General description

To run an analysis **you only need to use the Project class and FASTA files** containing genomes of different species. The steps of the analysis are:

1. Orthology inference
2. Gene tree creation
3. Trees reconciliation
4. Data visualization

Below are listed the classes and modules available 

## 1.1 Classes:

- [Project](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/docs/project.md) [Class]
  Helps you to configure and run analysis
  
- [GenesDict](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/docs/genesdict.md) [Class]
  Handles information of genes

## 1.2 Modules:

- evo_plot
- basic_functions [module]
  Functions used by all the other modules
- errors [module]
  Errors that may be raised durig analysis
- evolutionaryTree [module]
  Classes to represent ultrametreics (genes and species tree) and algorithms over them.
- nets [module]
  Functions to read and write the orthology relations in custom formats.
- order [module]
  Classes to travel on evolutionaryTree.Tree nodes
- stoerwagner [module]
  Compute all the minimun cuts of an un-directed weighted graph.REvolutionH-tl

# 2. Installation

## 2.1 Prerequisites

- [proteinortho](https://gitlab.com/paulklemm_PHD/proteinortho) (V >= 5.0)
- [Java](https://java.com/en/)
- [networkx](https://pypi.org/project/networkx/) (V < 2)

Note: You can install an old version of networkx with pip:

```bash
    pip install networkx==1.11 # for version 1.11
```

## 2.2 Download and use

Clone or download [this](https://gitlab.com/jarr.tecn/revolutionh-tl) repository. To use it:

1. go to the downloaded directory:

   ```bash
   cd revolutionh-tl
   ```

2. Write a python script or use package in a python interpreter and import the package.

Also see the [tutorial](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/tutorial.ipynb) for an example with real data, or read the [documentation](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/docs/documentation.md).