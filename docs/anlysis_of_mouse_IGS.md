# Data

You ca [find the data in drive](https://drive.google.com/drive/folders/18mj--t9FmNPQiHOttekC7ud0z1-uk8kG?usp=sharing). There you will find FASTA files containing protein sequences of 7 species downloaded from [ensemble  database](https://www.ensembl.org/index.html), as well as the orthology list generated using [proteinortho](https://gitlab.com/paulklemm_PHD/proteinortho) . The analyzed species are:

- Danio rerio
- Gallus gallus
- Homo sapiens
- Monodelphis domestica
- Mus musculus
- Pan troglodytes
- Rattus norvegicus

# Evolutionary analysis

1. Clone or download the [revolutionh-tl repository](https://gitlab.com/jarr.tecn/revolutionh-tl).

2. Go to `revolutionh-tl/` directory:

   ```bash
   cd revolutionh-tl/
   ```

3. Download [this folder](https://drive.google.com/drive/folders/18mj--t9FmNPQiHOttekC7ud0z1-uk8kG?usp=sharing) and put it inside `revolutionh-tl/` directory

4. Use [jupyter lab](https://jupyter.org/) to execute the [runMouseIGs.ipynb](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/runMouseIGs.ipynb) notebook (it is in the `revolutionh-tl/` directory).