# [REvolutionH-tl](https://gitlab.com/jarr.tecn/revolutionh-tl)

Reconstruction of Evolutionary Histories TooL

***

# revolutionhtl.Project [class]

Class that helps to handle analysis information, run methods, and interact with results.|

# Contents

1.  [How to use](#How to use)
2. [Analysis description](#Analysis description)
3. [Options](#Options)
4. [Methods](#Methods)

# How to use

All you need is:
1. **Configure your project**:

   ```python
   project= Project( [Options] )
   ```

2. **Run analysis**: 

   ```python
   project.run_analysis( [Options] )
   ```

See the [tutorial](./tutorial.ipynb) for a detailed example with real data.

# Analysis description

The analysis consists of 3 main steps:

1. Orthology graph creation.
   May use edges list or infer from FASTAS files using proteinortho.
   
2. Genes tree creation.
   Use Orthology graphs and optionally gene filters.
   
3. Trees reconciliation.
   Reconciliate the genes tree with an species tree provided.

Each of these steps depends on some data that is indicated as project attributes that you can configure at input options (`project= Project( [Options] )`) , these also determine path and name of the project's directory.

Project is created as a directory containing sub-directories with results and project information.

If the project does not exist, it is created.

If the project already exist it is loaded, but you only can reset the project attributes if 'reset' option is True.

The project has methods to run analysis, visualize and interact with results.

Options
--------

- **project_name** [str] (default: "project")
  Name of project directory.
  The projects directory will have this name.

- **project_path** [str] (default: "./")
  Absolute path where project directory will be created or loaded.
  Example: "/absolute/path/where/project/directory/will/be/created/or/loaded/"

- **reset** [bool] (default: False)
  True if you wan to reset projet parameters if exist.

- **fastas_path** [str]
  Absolute path to directory containing fasta files to analyze.
  Example: "/absolute/path/where/fasta/files/are/stored/"
  Will take all files with extensions:
  - .fa
  - .fasta
  - .faa
  - .FA
  - .FASTA
  - .FAA

- **fastas_label** [str]
  Specifies which species correspond to each fasta file.
  This parameter may be a reserved string or a path:
  - **Reserved strings**:
    - "ENSEMBLE"
      Assumes the fastas are named as the standard nomenclature of ensemble.
  - **Path**:
    Absolute path to .tsv file containing two columns:
    - file name
    - species
    Example path: "/absolute/path/where/is/file.tsv/"

- **genes_dict** [str]
  Information of genes.
  May be created automatically if data is available
  or may be provided if want to filter genes for
  custom analysis.
  - **Automatic creation**:
    This file will be created automatically if
    fastas_path and fastas_label provided and
    genes_dict not provided.
  - **path to .tsv file**
    This file must contain at least 2 columns:
    - gene ID
    - species

- **orthologs_list** [str]
  Contain orthology relations for orthology graph
  construction avoiding proteinortho. Can be specified
  with reserved strings or .tsv file:
  Reserved strings:
  - **"PROTEINORTHO"**
    This runs proteinortho for orthology prediction.
  - **Path to .tsv file containing two or three columns:**
    - gene a (mandatory)
  - gene b (mandatory)
    - weight (optional)

- **proteinortho_path** [string] (default: "proteinortho6.pl")
  Command to run proteinortho

- **proteinortho_p_algorithm** [str]
  Algorithm to run secuences comparation.
  Dependes on your sequences type:
  - "diamond" : for peptides (recomended)
  - "blastp"  : for peptides
  - "blastn"  : for nucleotides

- **species_tree** [str] (default: None)
  path to .newick file:
  The file must contain the species tree in newick format.
The allowed characters are:
  - from 'A' to 'Z' in upper an lower case
  - comma ','
  - parenthesis '(' and ')'
  - semicolon ';'
- NO spaces ' '
  - NO underscore '_'
  - NO new lines
  - The species must coincide with those in the genes tree
  **Example**:
    The newick tree:
    "((Human,Chimpanzee),(Mouse,Rattu),Opossium);"
    represents the species tree:
    -.
     |-Opossium
     |
     |-.
     | |-Rattu
     | |-Mouse
     |
     |-.
     | |-Chimpanzee
     | |-Human

Methods
--------

- **run_analysis**()Run the analysis or specific steps.
  
Inputs:
  
  - steps [int or list]:
    Specifies steps of analysis to run:
    One integer of { 0, 1, 2 ,3}
  or one sub-list of [1, 2, 3]
  
    If the integer is '0', then steps
    1,2 and 3 will be runed.
  
    The steps are:
    1. Create orthology graphs
    2. Create gene trees
    3. reconciliate gene trees
  
- **loadGenesdict**()
  Returns the `genesdict` object with dictionary specified by 'genes_dict' attribute and obtain information from fastas if it is possible. See also the [GenesDict](https://gitlab.com/jarr.tecn/revolutionh-tl/-/blob/master/docs/genesdict.md) class.
  
- **getTree( ID={int or str} )**
  returns a Tree object associated to an ID.

- **get_valid_trees_ID()**
  returns list of congruent trees ID

- **get_valid_trees_list()**
  returns list of congruent trees

- **getNewikcFileName( ID={int or str} )**
  Returns file name asociated to an ID.

- **getNewickFilePath( ID={int or str} )**
  Returns file path asociated to an ID.

- **getNewickString( ID={int or str} )**
  Returns a genes or species tree in newick format.