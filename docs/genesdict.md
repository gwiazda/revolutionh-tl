# [REvolutionH-tl](https://gitlab.com/jarr.tecn/revolutionh-tl)

Reconstruction of Evolutionary Histories TooL

***

# revolutionhtl.GenesDict [class]

Class that helps to handle analysis information, run methods, and interact with results.