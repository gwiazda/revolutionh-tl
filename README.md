# REvolutionH-tl

**Reconstruction of Evolutionary Histories Tool**

Bioinformatics tool for the inference, reconciliation, and analysis of event-labeled gene tree describing the evolutionary history of genes among different genomes. 

Bioinformatics team:
- J. Antonio R. R. [jarr.tecn@gmail.com] (UNAM)
- Maribel Hernandez Rosales [maribel.hr@cinvestav.mx ] (CINVESTAV)

****

# Links

- [RevolutionH-tl](#RevolutionH-tl).
- [Tutorial](./tutorial.ipynb).
- [Documentation](docs/documentation.md).
- [Analysis of mouse intronless genes evolution](docs/anlysis_of_mouse_IGS.md).

# Outline

1. [A brief description](#A brief description)
2. [Genes evolutionary history: event labeled genes tree](#Genes evolutionary history: event labeled genes tree)
3. [Reconciliation of trees](#Reconciliation of trees)
4. [Main workflow](#Main workflow)

# 1. A brief description

This package analyzes the evolutionary history of genes in terms of **speciation** and **duplication** events represented in a gene tree coupled with a **trees reconciliation**.

The three main steps of the analysis are:

1. Orthology inference.
2. Obtaining a gene tree.
3. Reconciliation of gene and species trees.

This package allows to summarize and visualize the data obtained from the trees reconciliation. For example: The evolutionary history of the mouse beta-protocadherins infered from 7 proteomes obtained from [ensembl database](https://www.ensembl.org/).

![Infered evolutionary history of mouse beta protocadherins.](docs/images/pipe_recon.svg)

![Reconciliation tree summarizing the evolution of mouse beta protocadherins.](docs/images/protocadherins_reconciliation_tree.svg)

# 2. Genes evolutionary history: event labeled genes tree

Gene histories must be presented in the form of scenarios that comprise several types of elementary events. This history can be considered as a tree where genes and inner nodes represent evolutionary events.

In the next figure, anexample of a gene tree depicting the two events:

- Speciation (red circle)
- Gene duplication (blue diamond)

![](docs/images/genes_tree.png)

# 3. Trees reconciliation

The topology of a gene tree is compared with that of a species tree. They are reconciled by postulating the minimal possible number of duplication and gene-loss events in the evolution of the given gene.

Reconciliation can be done if and only if the species tree displays all triplets of gene trees whose root is a speciation event.

Reconciliation shows where duplication and gene-loss events occur.

In the next figure, an example of the reconciliation of the gene and species trees.

![](docs/images/genes_tree_recon.png)

# 4. Main workflow

This tool allows the **inference and the study of the evolutionary history of an extensive (co-)ortholog gene groups among different species** by approximating an **orthology graph** (where nodes represent genes, and there is an edge between two nodes if they are predicted to be orthologous genes).

Then, the **orthology graph is transformed to a gene-tree**, where leaves represent sequences (genes, proteins, transcripts,etc.,); and the inner nodes represent the evolutionary events (speciation and duplication events). This tree is obtained by applying an algorithm for *modular decomposition* proposed by Christian Capelle et. al. (1994) to the *Proteinortho* graph, and a *min-cut* algorithm proposed by Mechthild Stoer and Frank Wagner (1997) that ensures the graph is a cograph by removing or adding orthology relationships. This last condition relies on the theory of symbolic ultrametric aborded by Hellmuth M, Hernandez-Rosales et. al. (2012): *a relation on a set of genes is an orthology relation if and only if it is a cograph*.

Finally, the tool  **reconciles the gene trees with the species tree** by mapping each gene trees to the species tree (where the leaves are existent species and inner nodes *ancestral species* i.e. species which existed and from which the current species descend). this process methods the theory and algorithms published in Hernandez-Rosales and Hellmuth M. et. al. (2012). When analyzing the process of mapping a gene to species trees: *S is a species tree for the genes tree T, if and only, if S displays all rooted triples of T that have three distinct species as their leaves and are rooted in a speciation vertex*, and also denotes that the *gene trees convey a large amount of information on underlying species trees, even for a large percentage of gene losses*.

It is possible to infer potential gene families with the tool [Proteinortho](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-124#citeas) (developed by Lechner M and Findeiß S.  *et al.* in (2001)).
